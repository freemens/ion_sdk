# Changelog

v0.1.1

- Changelog Initiated.

v0.2.0

- Version Release for public use.
- Refactors components to assets for uniform langauge.
- Adds a ``setupClient`` function to setup an ``EdisonClient``. Prompts the user to input the API Key in a password text-box.

v0.2.1

- Adds documentation for factory, factoryModel.
- Adds coverage badge.
- Updates requirements.txt to require version greater than initial.
- Adds flag in upload methods to run them quietly.

v0.3.0

- Adds support for single tenent systems using environment variables.
- Adds erase all data functionality.
- Improves error handling with more informative errors.
- Adds ability to allow for a dirty delete when updating data using the file method.
- Adds a decimator tool in the toolbox.
- Adds ability to update array type data using the direct insert method.
- Unifies column names for sensors for the update by direct insert and update by file to use sensor names only. Thus, direct insert no longer takes sensor code as an input.

v0.3.1

- Fixes issue related to the link in setupClient hitting an error 404.
- Fixes issue in erase all data that prevent erase from taking place.
- Updates README.md to ensure Colab users can use the magic commands for setting environment variables.
- Adds rate-limit (1 hit per 10 seconds) for all methods that create a worker on the platform.
- Updates the printing link for the correct domain.
