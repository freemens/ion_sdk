from datetime import timedelta
from ion_sdk.edison_api.models.factory import (
    EdisonComponent,
    getCellModel,
    getParameterValue,
)
from ion_sdk.tools.sim import Sensor
from ion_sdk.tools.utils import float_to_str
import pandas as pd
import string
import sys
from time import sleep
from ion_sdk.tools.utils import progress
import numpy as np
import math as ma
from plotly import __version__
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import plotly.graph_objs as go
import datetime
from tqdm import tqdm


def getTime(df: pd.DataFrame, initDuration=0.0, **kwargs):
    df["time"] = df.index
    df["currentDuration"] = (df["time"] - df["time"][0]).dt.total_seconds() + initDuration
    df["prevDuration"] = df["currentDuration"].shift(1)
    df["deltaT"] = df["time"].diff(periods=1).dt.total_seconds()
    return df


def dataframeFromSensors(sensors: Sensor):
    dfList = []

    for key in sensors:
        df = pd.DataFrame(sensors[key].time, columns=["date"])
        df[key] = sensors[key].data
        sigVarStr = float_to_str(sensors[key].sigVarTh)
        significantDecimals = sigVarStr[::-1].find(".")
        df[key] = df[key].round(significantDecimals)
        df = df.drop_duplicates("date")
        df.set_index("date", inplace=True)
        dfList.append(df)

    df = pd.concat(dfList, axis=1, sort="False")  # Concat all the columns in the frame, aligning by time index
    df = df.groupby(level=0, axis=1).first()  # Merge same named columns by grouping using none n.a values (first())
    dfList.clear()

    return df


def plotSensors(sensors: Sensor, df, plotTitle="Plot", width=None):

    myGraphs = []
    axisLayout = {}
    axisPosition = 0.0
    # niceDate = []

    # for key in sensors:
    #     if sensors[key].plotted==1:
    #         df[key] = sensors[key].data
    #         sigVarStr=float_to_str(sensors[key].sigVarTh)

    #         significantDecimals=sigVarStr[::-1].find('.')
    #         df[key]=df[key].round(significantDecimals)
    sensorSpace = 0.08
    sensorCnt = 0
    for key in sensors:
        if sensors[key].plotted == 1:
            sensorCnt += 1

    for key in sensors:
        if sensors[key].plotted == 1:
            if sensors[key].axis == 1:
                traceAxis = "y"
                axisParameter = "yaxis"
            else:
                traceAxis = "y" + str(sensors[key].axis)
                axisParameter = "yaxis" + str(sensors[key].axis)
            trace = go.Scatter(
                x=df.index,
                y=df[key].ffill(),
                yaxis=traceAxis,
                name=key + " " + sensors[key].unit,
            )

            if axisParameter not in axisLayout:
                if axisParameter == "yaxis":
                    axisLayout["yaxis"] = dict(title=key, overlaying=None, anchor="free")
                else:
                    axisLayout[axisParameter] = dict(
                        title=key + " (" + sensors[key].unit + ")",
                        overlaying="y",
                        position=1 - axisPosition,
                        anchor="free",
                        side="right",
                    )
                    axisPosition += sensorSpace
            myGraphs.append(trace)

    layout = go.Layout(
        title=plotTitle,
        template="plotly_white",
        autosize=True if width is None else False,
        width=width if width is not None else None,
        xaxis=dict(title="time", domain=[0.0, 1.0 - axisPosition]),
        **axisLayout,
    )
    return go.Figure(data=myGraphs, layout=layout)


def decimate_data(series: pd.Series, val_threshold: float = 1e-3, time_threshold: datetime.timedelta = datetime.timedelta(seconds=3600)):
    """
    A function to decimate data that is to be sent to the Altergo platform in order to reduce data transfer size.
    Based on the value of ``val_threshold`` and ``time_threshold``, points will registered. A point will be registered difference between current recorded value and previously recorded value in higher than ``val_threshold`` or the difference betwee the current timestamp and the previously recorded timestamp is greater than ``time_threshold``.

    ::: note

    This method is only valid for one series at a time.

    :::

    Args:
        series (pd.Series): The series data that needs to be decimated.
        val_threshold (float): A value threshold that must be exceeded by the difference between the current and the previously
            recorded value, in order to register the point after decimation. Defaults to 1e-3.

        time_threshold (datetime.timedelta): A time-based threshold that must be exceeded by the difference between the current and the
        previously recorded timestamp, in order to register the point after decimation.

    Returns:
        pd.DataFrame with decimated data where decimation is performed according the threshold values provided.

    Example:

    >>> np.random.seed(1234)
    >>> ser = pd.Series(
            data = [x + np.random.randn() for x in range(0,100,1)],
            index = [x for x in pd.date_range(datetime.datetime(2021,7,10), periods = 100, freq="1S")],
        )
    >>> df = decimate_data(ser, val_threshold = 1e0, time_threshold = datetime.timedelta(seconds = 5))
    >>> ser.size, df.size
    (100,66)
    """

    cum_time = timedelta(days=999)
    idxs = [series.index[0]]
    vals = [series.values[0]]

    for idx, val in tqdm(series.iteritems(), total=series.size):
        cum_time = idx - idxs[-1]
        # Decide to include the point based on either of two thresholds: time based on value based. If either are True, include the point.
        if (cum_time >= time_threshold) or (abs(val - vals[-1]) >= val_threshold):
            idxs.append(idx)
            vals.append(val)
            cum_time = timedelta()

    if series.name is None:
        return pd.DataFrame({"Decimated Series": {x: y for x, y in zip(idxs, vals)}})
    else:
        return pd.DataFrame({series.name: {x: y for x, y in zip(idxs, vals)}})
