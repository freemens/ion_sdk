import pandas as pd


class Sensor:
    def __init__(
        self,
        name="Default Sensor",
        data=[],
        time=[],
        unit="#",
        axis=1,
        lastSigVal=0.0,
        sigVarTh=0.0,
        synced=0,
        plotted=1,
    ):

        self.name = name
        self.data = data
        self.time = time
        self.unit = unit
        self.axis = axis
        self.synced = synced
        self.lastSigVal = lastSigVal
        self.sigVarTh = sigVarTh
        self.plotted = plotted
        self.previousData = 0.0
        self.previousTime = 0.0
        self.previousDataToLog = 0
        self.noValueCnt = 0

    def significantAppend(self, newValue, newtime):

        if abs(newValue - self.lastSigVal) >= self.sigVarTh:
            if self.previousDataToLog == 1:
                self.data.append(self.previousData)
                self.time.append(self.previousTime)
                self.previousDataToLog = 0

            self.lastSigVal = newValue
            self.data.append(newValue)
            self.time.append(newtime)

        else:
            self.previousData = newValue
            self.previousTime = newtime
            self.noValueCnt += 1
            if self.noValueCnt > 5:
                self.previousDataToLog = 1
                self.noValueCnt = 0
            pass
            # self.data.append(None)
            # self.time.append(newtime)
