# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = sensor_data_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Any, List, TypeVar, Callable, Type, cast
from datetime import datetime


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class ID:
    sensor_id: str
    sensor_position: int

    @staticmethod
    def from_dict(obj: Any) -> "ID":
        assert isinstance(obj, dict)
        sensor_id = from_str(obj.get("sensorId"))
        sensor_position = int(from_str(obj.get("sensorPosition")))
        return ID(sensor_id, sensor_position)

    def to_dict(self) -> dict:
        result: dict = {}
        result["sensorId"] = from_str(self.sensor_id)
        result["sensorPosition"] = from_str(str(self.sensor_position))
        return result


@dataclass
class SensorRecord:
    ts: int
    val: float

    @staticmethod
    def from_dict(obj: Any) -> "SensorRecord":
        assert isinstance(obj, dict)
        ts = from_int(obj.get("ts"))
        val = from_float(obj.get("val"))
        return SensorRecord(ts, val)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ts"] = from_int(self.ts)
        result["val"] = to_float(self.val)
        return result


@dataclass
class Datum:
    id: ID
    sensor_records: List[SensorRecord]

    @staticmethod
    def from_dict(obj: Any) -> "Datum":
        assert isinstance(obj, dict)
        id = ID.from_dict(obj.get("_id"))
        sensor_records = from_list(
            SensorRecord.from_dict, obj.get("sensorRecords")
        )
        return Datum(id, sensor_records)

    def to_dict(self) -> dict:
        result: dict = {}
        result["_id"] = to_class(ID, self.id)
        result["sensorRecords"] = from_list(
            lambda x: to_class(SensorRecord, x), self.sensor_records
        )
        return result


@dataclass
class SensorDatum:
    serial_number: str
    data: List[Datum]

    @staticmethod
    def from_dict(obj: Any) -> "SensorDatum":
        assert isinstance(obj, dict)
        serial_number = from_str(obj.get("serialNumber"))
        data = from_list(Datum.from_dict, obj.get("data"))
        return SensorDatum(serial_number, data)

    def to_dict(self) -> dict:
        result: dict = {}
        result["serialNumber"] = from_str(self.serial_number)
        result["data"] = from_list(lambda x: to_class(Datum, x), self.data)
        return result


def sensor_data_from_dict(s: Any) -> List[SensorDatum]:
    return from_list(SensorDatum.from_dict, s)


def sensor_data_to_dict(x: List[SensorDatum]) -> Any:
    return from_list(lambda x: to_class(SensorDatum, x), x)
