import ion_sdk.edison_api.models.factoryModel as fm
from dataclasses import dataclass, field
from typing import Any, List, TypeVar, Type, Union, cast, Callable, Optional

from datetime import datetime
from enum import Enum
import dateutil.parser
import string
from pandas import DataFrame

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class EdisonComponent(fm.EdisonGenericComponent):
    """
    EdisonComponent is a child class of the EdisonGenericComponent. It takes the same arguments as the EdisonGenericComponent.
    EdisonComponent is the basic asset class for building assets on Edison.

    The dictionary must contain the following ``keys`` (and the expected ``value`` type):
        * "id" (int, or ``None``):
        * "iot_data" (dict or ``None``):
        * "serial_number" (str, or ``None``):
        * "model" (Model, or ``None``):
        * "current_state" (CurrentState, or ``None``):
        * "tags" (list[Tag], or ``None``):
        * "df" ([DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)):
        * "dfList" ([DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)):

    Args:
        id (int, or ``None``):
        iot_data (dict or ``None``):
        serial_number (str, or ``None``):
        model (Model, or ``None``): Factory Model which forms the base for the asset.
        current_state (CurrentState, or ``None``):
        tags (list[Tag], or ``None``):
        df ([DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html), optional):
        dfList ([DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html), optional):
    """

    # ? Should there be a custom method for __repr__ instead of the default method created by dataclass?

    df: Optional[DataFrame] = None
    dfList: Optional[DataFrame] = None

    @staticmethod
    def from_dict(obj: dict) -> "EdisonComponent":
        """
        Create an EdisonComponent from a dictionary. The dictionary must contain the required arguments as keys within the dictionary and the expected type as the values.

        Args:
            obj (dict): Dictionary containing the required arguments as keys and the expected type as values. This dictionary is used to create an EdisonComponent.

        Returns:
            EdisonComponent: EdisonComponent (asset-like) object which is local to your environment.
        """
        # TODO: Add example in the docstring.

        assert isinstance(obj, dict)  # ? Changed the argument type to force a dictionary. This line may be redundant.
        id = fm.from_union([fm.from_int, fm.from_none], obj.get("id"))
        iot_data = fm.from_union([fm.IotData.from_dict, fm.from_none], obj.get("iotData"))
        serial_number = fm.from_union([fm.from_str, fm.from_none], obj.get("serialNumber"))
        model = fm.from_union([fm.Model.from_dict, fm.from_none], obj.get("model"))
        current_state = fm.from_union([fm.CurrentState.from_dict, fm.from_none], obj.get("currentState"))
        tags = fm.from_union(
            [lambda x: fm.from_list(fm.Tag.from_dict, x), fm.from_none],
            obj.get("tags"),
        )
        return EdisonComponent(id, serial_number, iot_data, model, current_state, tags, None, [])


def getParameterValue(model: fm.Model, valName: str, valType: str) -> Union[str, float]:
    """
    Get the parameter value for a given parameter name, and value type from the given model.

    Args:
        model (Model): Factory Model from which the parameter value is required.
        valName (str): Name of the parameter for which the value is required.
        valType (str): Type of the parameter for which the value is required. e.g. Nom, Max, Min, etc. Refer to the model created on Edison for more details.

    Returns:
        (str, or float): Value of the parameter. The value may be a string or a float value based on the parameter. ``"nan"`` is returned if parameter name is not found.
    """
    # ? Raise Exception instead of returning NaN?
    # TODO: Add example in the docstring.

    for p in model.parameters:
        if p.parameter_model.name == valName:
            for v in p.parameter_values:
                if v.parameter_name_prefix.name == valType:
                    if v.value.isalpha():  # Dropping the is True as the returned value is boolean
                        return v.value
                    else:
                        return float(v.value)

    return "nan"


def getCellModel(model: fm.Model):
    if model.category.name == "Cell":
        return model
    else:
        for c in model.child_component_models:
            cellModel = getCellModel(c.model)
            if cellModel is not None:
                return cellModel
    return None


def getSensorCodeByName(x: fm.Model, name: str) -> Optional[str]:
    """
    Get the sensor code (three alphanumeric characters) using the sensor name.

    Args:
        x (Model): Facotry Model which contains the sensors.
        name (str): Sensor name.

    Returns:
        str, or None: Sensor code (three alphanumeric characters).
    """
    # TODO: Add example in the docstring.

    return next((x for x in x.sensors if x.sensor_model.name == name), None).sensor_model.code


def getSensorNameByCode(x: fm.Model, code: str) -> Optional[str]:
    """
    Get the sensor name using the sensor code (three alphanumeric characters)

    Args:
        x (Model): Facotry Model which contains the sensors.
        code (str): Sensor code

    Returns:
        str, or None: Sensor name.
    """
    # TODO: Add example in the docstring.

    return next((x for x in x.sensors if x.sensor_model.code == code), None).sensor_model.name


def getSensorByName(x: fm.Model, name: str) -> Optional["fm.Sensor"]:
    """
    Get the sensor using the sensor name.

    Args:
        x (Model): Facotry Model which contains the sensors.
        name (str): Sensor name.

    Returns:
        Sensor, or None: Factory Sensor
    """
    # TODO: Add example in the docstring.

    return next((x for x in x.sensors if x.sensor_model.name == name), None)


def getSensorByCode(x: fm.Model, code):
    """
    Get the sensor using the sensor code.

    Args:
        x (Model): Facotry Model which contains the sensors.
        code (str): Sensor code.

    Returns:
        Sensor, or None: Factory Sensor
    """
    # TODO: Add example in the docstring.

    return next((x for x in x.sensors if x.sensor_model.code == code), None)
