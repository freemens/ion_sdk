# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = edison_generic_component_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, Any, List, TypeVar, Type, Union, cast, Callable
from datetime import datetime
from enum import Enum
import dateutil.parser

from pandas import DataFrame
import pandas

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except Exception:  # Avoiding a bare except
            pass  # ? Add a print statement to describe exception
    assert False


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


# Adding a missing function. Could re-use from_str too.
def to_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def is_type(t: Type[T], x: Any) -> T:
    assert isinstance(x, t)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


# Adding a missing function. Could re-use from_bool too.
def to_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x



@dataclass
class ComponentModelInterfaceCompatibleUnit:
    id: int
    model: 'Model'

    @staticmethod
    def from_dict(obj: Any) -> 'ComponentModelInterfaceCompatibleUnit':
        assert isinstance(obj, dict)
        id = from_int(obj.get("id"))
        model = Model.from_dict(obj.get("model"))
        return ComponentModelInterfaceCompatibleUnit(id, model)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_int(self.id)
        result["model"] = to_class(Model, self.model)
        return result


@dataclass
class ComponentModelInterface:
    id: int
    name: str
    quantity: int
    is_virtual: bool
    component_model_interface_compatible_units: List[ComponentModelInterfaceCompatibleUnit]

    @staticmethod
    def from_dict(obj: Any) -> 'ComponentModelInterface':
        assert isinstance(obj, dict)
        id = from_int(obj.get("id"))
        name = from_str(obj.get("name"))
        quantity = from_int(obj.get("quantity"))
        is_virtual = from_bool(obj.get("isVirtual"))
        component_model_interface_compatible_units = from_list(ComponentModelInterfaceCompatibleUnit.from_dict, obj.get("componentModelInterfaceCompatibleUnits"))
        return ComponentModelInterface(id, name, quantity, is_virtual, component_model_interface_compatible_units)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_int(self.id)
        result["name"] = from_str(self.name)
        result["quantity"] = from_int(self.quantity)
        result["isVirtual"] = from_bool(self.is_virtual)
        result["componentModelInterfaceCompatibleUnits"] = from_list(lambda x: to_class(ComponentModelInterfaceCompatibleUnit, x), self.component_model_interface_compatible_units)
        return result


@dataclass
class ChildrenComponentModelInterface:
    id: int
    component_model_interface: ComponentModelInterface

    @staticmethod
    def from_dict(obj: Any) -> 'ChildrenComponentModelInterface':
        assert isinstance(obj, dict)
        id = from_int(obj.get("id"))
        component_model_interface = ComponentModelInterface.from_dict(obj.get("componentModelInterface"))
        return ChildrenComponentModelInterface(id, component_model_interface)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_int(self.id)
        result["componentModelInterface"] = to_class(ComponentModelInterface, self.component_model_interface)
        return result

@dataclass
class Datetime:
    """
    A class to handle Datetime elements.

    Args:
        date (datetime, optional): Required datetime. Defaults to ``None``. Note that this is not the same as the datetime module which is a part of Python.
        timezone_type (int, optional): Defaults to ``None``.
        timezone (str, optional): Time zone for the date. Defaults to ``None``.
    """

    # ? Why not use timezone-aware datetime?

    date: Optional[datetime] = None
    timezone_type: Optional[int] = None
    timezone: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Datetime":
        """
        Generate a ``Datetime`` object from a dictionary containing the arguments as keys.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Datetime``: The ``Datetime`` object.
        """
        assert isinstance(obj, dict)  # ? Is this required if the typing is forced to dict?
        date = from_union([from_datetime, from_none], obj.get("date"))
        timezone_type = from_union([from_int, from_none], obj.get("timezone_type"))
        timezone = from_union([from_str, from_none], obj.get("timezone"))
        return Datetime(date, timezone_type, timezone)

    def to_dict(self) -> dict:
        """
        Convert the ``Datetime`` object into a dictionary.

        Returns:
            dict: Dictionary containing the ``date``, ``timezone_type``, and ``timezone`` as keys.
        """
        result: dict = {}
        result["date"] = from_union([lambda x: x.isoformat(), from_none], self.date)
        result["timezone_type"] = from_union([from_int, from_none], self.timezone_type)
        result["timezone"] = from_union([from_str, from_none], self.timezone)
        return result


@dataclass
class Status:
    """
    A class to handle ``Status``.

    Args:
        id (int, optional): ``Status`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Status``. Defaults to ``None``.
        type (str, optional): Type of the ``Status``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    type: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Status":
        """
        Generate a ``Status`` object from a dictionary containing the arguments as keys.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Status``: The ``Status`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        type = from_union([from_str, from_none], obj.get("type"))
        return Status(id, name, type)

    def to_dict(self) -> dict:
        """
        Converts a ``Status`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``type`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["type"] = from_union([from_str, from_none], self.type)
        return result


@dataclass
class CurrentState:
    """
    A class to handle ``CurrentState``.

    Args:
        id (int, optional): ``CurrentState`` ID. Defaults to ``None``.
        location (str, optional): Location for the ``CurrentState``. Defaults to ``None``.
        description (str, optional): Description of the ``CurrentState``. Defaults to ``None``.
        current_state_datetime (``Datetime``, optional): ``Datetime`` for the ``CurrentState``. Defaults to ``None``.
        status (``Status``, optional): ``Status`` of the ``CurrentState``. Defaults to ``None``.
        status_existential (``Status``, optional): Existential ``Status`` of the ``CurrentState``. Defaults to ``None``.
        status_functional (``Status``, optional): Functional ``Status`` of the ``CurrentState``. Defaults to ``None``.
    """

    id: Optional[int] = None
    location: Optional[str] = None
    description: Optional[str] = None
    current_state_datetime: Optional[Datetime] = None
    status: Optional[Status] = None
    status_existential: Optional[Status] = None
    status_functional: Optional[Status] = None
    child_components = None
    
    @staticmethod
    def from_dict(obj: dict) -> "CurrentState":
        """
        Generate a ``CurrentState`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``CurrentState``: The ``CurrentState`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        location = from_union([from_str, from_none], obj.get("location"))
        description = from_union([from_str, from_none], obj.get("description"))
        current_state_datetime = from_union([Datetime.from_dict, from_none], obj.get("datetime"))
        status = from_union([Status.from_dict, from_none], obj.get("status"))
        status_existential = from_union([Status.from_dict, from_none], obj.get("statusExistential"))
        status_functional = from_union([Status.from_dict, from_none], obj.get("statusFunctional"))
        child_components = from_union([lambda x: from_list(EdisonGenericComponent.from_dict, x),from_none], obj.get('childComponents'))
        
        return CurrentState(
            id,
            location,
            description,
            current_state_datetime,
            status,
            status_existential,
            status_functional,
            child_components
        )

    def to_dict(self) -> dict:
        """
        Converts a ``CurrentStatue`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``location``, ``description``, ``datetime``, ``status``, ``statusExistental``, and ``statusFunctional`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["location"] = from_union([from_str, from_none], self.location)
        result["description"] = from_union([from_str, from_none], self.description)
        result["datetime"] = from_union(
            [lambda x: to_class(Datetime, x), from_none],
            self.current_state_datetime,
        )
        result["status"] = from_union([lambda x: to_class(Status, x), from_none], self.status)
        result["statusExistential"] = from_union([lambda x: to_class(Status, x), from_none], self.status_existential)
        result["statusFunctional"] = from_union([lambda x: to_class(Status, x), from_none], self.status_functional)
        return result


@dataclass
class IotData:
    """
    A class to handle ``IotData``.

    Args:
        id (int, optional): ``IotData`` ID. Defaults to ``None``.
        last_update (``Datetime``, optional): Last Datetime when the ``IotData`` was updated. Defaults to ``None``.
        shadow (str, optional):
        latitude (float, optional): Latitude of the ``IotData``. Defaults to ``None``.
        longitude (float, optional): Longitude of the ``IotData``. Defaults to ``None``.
    """

    id: Optional[int] = None
    last_update: Optional[Datetime] = None
    shadow: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None

    @staticmethod
    def from_dict(obj: dict) -> "IotData":
        """
        Generate an ``IotData`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``IotData``: The ``IotData`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        last_update = from_union([Datetime.from_dict, from_none], obj.get("lastUpdate"))
        shadow = from_union([from_str, from_none], obj.get("shadow"))
        latitude = from_union([from_float, from_none], obj.get("latitude"))
        longitude = from_union([from_float, from_none], obj.get("longitude"))
        return IotData(id, last_update, shadow, latitude, longitude)

    def to_dict(self) -> dict:
        """
        Converts a ``IotData`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``lastUpdate``, ``shadow``, ``latitude``, and ``longitude`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["lastUpdate"] = from_union([lambda x: to_class(Datetime, x), from_none], self.last_update)
        result["shadow"] = from_union([from_str, from_none], self.shadow)
        result["latitude"] = from_union([to_float, from_none], self.latitude)
        result["longitude"] = from_union([to_float, from_none], self.longitude)
        return result


@dataclass
class Category:
    """
    A class to handle ``Category``.

    Args:
        id (int, optional): ``Category`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Category``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Category":
        """
        Generate an ``Category`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Category``: The ``Category`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        return Category(id, name)

    def to_dict(self) -> dict:
        """
        Converts a ``Category`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, and ``name`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        return result


@dataclass
class Variable:
    """
    A class to handle ``Category``.

    Args:
        id (int, optional): ``Variable`` ID. Defaults to ``None``.
        real_id (int, optional): Real ``Variable`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Variable``. Defaults to ``None``.
        type (int, optional): Type of ``Variable``. Defaults to ``None``.
    """

    id: Optional[int] = None
    real_id: Optional[int] = None
    name: Optional[str] = None
    type: Optional[int] = None

    @staticmethod
    def from_dict(obj: dict) -> "Variable":
        """
        Generate an ``Variable`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Variable``: The ``Variable`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        real_id = from_union([from_int, from_none], obj.get("realId"))
        name = from_union([from_str, from_none], obj.get("name"))
        type = from_union([from_int, from_none], obj.get("type"))
        return Variable(id, real_id, name, type)

    def to_dict(self) -> dict:
        """
        Converts a ``Variable`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``realId``, ``name``, and ``type`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["realId"] = from_union([from_int, from_none], self.real_id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["type"] = from_union([from_int, from_none], self.type)
        return result


@dataclass
class ConfigurationValue:
    """
    A class to handle ``ConfigurationValue``.

    Args:
        id (int, optional): ``ConfigurationValue`` ID. Defaults to ``None``.
        value (str, optional): Value of the ``ConfigurationValue``. Defaults to ``None``.
        variable (Variable, optional)
    """

    id: Optional[int] = None
    value: Optional[str] = None
    variable: Optional[Variable] = None

    @staticmethod
    def from_dict(obj: dict) -> "ConfigurationValue":
        """
        Generate a ``ConfigurationValue`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ConfigurationValue``: The ``ConfigurationValue`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        value = from_union([from_str, from_none], obj.get("value"))
        variable = from_union([Variable.from_dict, from_none], obj.get("variable"))
        return ConfigurationValue(id, value, variable)

    def to_dict(self) -> dict:
        """
        Converts a ``ConfigurationValue`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``value``, and ``variable`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["value"] = from_union([from_str, from_none], self.value)
        result["variable"] = from_union([lambda x: to_class(Variable, x), from_none], self.variable)
        return result


@dataclass
class ConfigurationContent:
    """
    A class to handle ``ConfigurationContent``.

    Args:
        id (int, optional): ``ConfigurationContent`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``ConfigurationContent``. Defaults to ``None``.
        configuration_content_datetime (int, optional): Datetime of the ``ConfigurationContent``. Defaults to ``None``.
        author (str, optional): Author of the ``ConfigurationContent``. Defaults to ``None``.
        message (str, optional): Message in the ``ConfigurationContent``. Defaults to ``None``.
        configuration_values (list of ``ConfigurationValues``, optional): A list of ``ConfigurationValues``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    configuration_content_datetime: Optional[int] = None  # ? Shouldn't this be datetime?
    author: Optional[str] = None
    message: Optional[str] = None
    configuration_values: Optional[List[ConfigurationValue]] = None

    @staticmethod
    def from_dict(obj: dict) -> "ConfigurationContent":
        """
        Generate a ``ConfigurationContent`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ConfigurationContent``: The ``ConfigurationContent`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        configuration_content_datetime = from_union([from_int, from_none], obj.get("datetime"))
        author = from_union([from_str, from_none], obj.get("author"))
        message = from_union([from_str, from_none], obj.get("message"))
        configuration_values = from_union(
            [lambda x: from_list(ConfigurationValue.from_dict, x), from_none],
            obj.get("configurationValues"),
        )
        return ConfigurationContent(
            id,
            name,
            configuration_content_datetime,
            author,
            message,
            configuration_values,
        )

    def to_dict(self) -> dict:
        """
        Converts a ``ConfigurationContent`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, ``datetime``, ``author``, ``message`` and ``configurationValues`` as keys.
        """

        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["datetime"] = from_union([from_int, from_none], self.configuration_content_datetime)
        result["author"] = from_union([from_str, from_none], self.author)
        result["message"] = from_union([from_str, from_none], self.message)
        result["configurationValues"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(ConfigurationValue, x), x),
                from_none,
            ],
            self.configuration_values,
        )
        return result


@dataclass
class ConfigurationFlavor:
    """
    A class to handle ConfigurationFlavor.

    Args:
        id (int, optional): ``ConfigurationFlavor`` ID. Defaults to ``None``.
        type (str, optional): Type of the ``ConfigurationFlavor``. Defaults to ``None``.
        configuration_contents (list of ``ConfigurationContent``, optional): A list of ``ConfigurationContent``. Defaults to ``None``.
    """

    id: Optional[int] = None
    type: Optional[str] = None
    configuration_contents: Optional[List[ConfigurationContent]] = None

    @staticmethod
    def from_dict(obj: dict) -> "ConfigurationFlavor":
        """
        Generate a ``ConfigurationFlavor`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ConfigurationFlavor``: The ``ConfigurationFlavor`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        type = from_union([from_str, from_none], obj.get("type"))
        configuration_contents = from_union(
            [
                lambda x: from_list(ConfigurationContent.from_dict, x),
                from_none,
            ],
            obj.get("configurationContents"),
        )
        return ConfigurationFlavor(id, type, configuration_contents)

    def to_dict(self) -> dict:
        """
        Converts a ``ConfigurationFlavor`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``type``, and ``configurationContents`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["type"] = from_union([from_str, from_none], self.type)
        result["configurationContents"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(ConfigurationContent, x), x),
                from_none,
            ],
            self.configuration_contents,
        )
        return result


@dataclass
class Configuration:
    """
    A class to handle ``Configuration``.

    Args:
        id (int, optional): ``Configuration`` ID. Defaults to ``None``.
        type (str, optional): Type of the ``Configuration``. Defaults to ``None``.
        configuration_flavors (list of ``ConfigurationFlavor``, optional): A list of ``ConfigurationFlavor``. Defaults to ``None``.
    """

    id: Optional[int] = None
    type: Optional[str] = None
    configuration_flavors: Optional[List[ConfigurationFlavor]] = None

    @staticmethod
    def from_dict(obj: dict) -> "Configuration":
        """
        Generate a ``Configuration`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Configuration``: The ``Configuration`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        type = from_union([from_str, from_none], obj.get("type"))
        configuration_flavors = from_union(
            [lambda x: from_list(ConfigurationFlavor.from_dict, x), from_none],
            obj.get("configurationFlavors"),
        )
        return Configuration(id, type, configuration_flavors)

    def to_dict(self) -> dict:
        """
        Converts a ``Configuration`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``type``, and ``configurationFlavors`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["type"] = from_union([from_str, from_none], self.type)
        result["configurationFlavors"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(ConfigurationFlavor, x), x),
                from_none,
            ],
            self.configuration_flavors,
        )
        return result


@dataclass
class Image:
    """
    A class to handle ``Image``.

    Args:
        id (int, optional): ``Image`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Image``. Defaults to ``None``.
        file (str, optional): Filepath to the ``image``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    file: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Image":
        """
        Generate an ``Image`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Image``: The ``Image`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_none, from_str], obj.get("name"))
        file = from_union([from_str, from_none], obj.get("file"))
        return Image(id, name, file)

    def to_dict(self) -> dict:
        """
        Converts a ``Image`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``file`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_none, from_str], self.name)
        result["file"] = from_union([from_str, from_none], self.file)
        return result


@dataclass
class ParameterModel:
    """
    A class to handle ``ParameterModel``.

    Args:
        id (int, optional): ``ParameterModel`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``ParameterModel``. Defaults to ``None``.
        si_unit (``Category``, optional): SI Unit of the ``ParameterModel``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    si_unit: Optional[Category] = None

    @staticmethod
    def from_dict(obj: dict) -> "ParameterModel":
        """
        Generate a ``ParameterModel`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ParameterModel``: The ``ParameterModel`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        si_unit = from_union([Category.from_dict, from_none], obj.get("siUnit"))
        return ParameterModel(id, name, si_unit)

    def to_dict(self) -> dict:
        """
        Converts a ``ParameterModel`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``siUnit`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["siUnit"] = from_union([lambda x: to_class(Category, x), from_none], self.si_unit)
        return result


class Name(Enum):
    EMPTY = ""
    M = "m"


@dataclass
class SiPrefix:
    """
    A class to handle ``SiPrefix``.

    Args:
        id (int, optional): ``SiPrefix`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        value (float, optional): Value of the ``SiPrefix``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    value: Optional[float] = None

    @staticmethod
    def from_dict(obj: dict) -> "SiPrefix":
        """
        Generate an ``SiPrefix`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``SiPrefix``: The ``SiPrefix`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        value = from_union([from_float, from_none], obj.get("value"))
        return SiPrefix(id, name, value)

    def to_dict(self) -> dict:
        """
        Converts an ``SiPrefix`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``value`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["value"] = from_union([to_float, from_none], self.value)
        return result


@dataclass
class ParameterValue:
    """
    A class to handle ParameterValue.

    Args:
        id (int, optional): ``ParameterValue`` ID. Defaults to ``None``.
        value (str, optional): Value of the ``ParameterValue``. Defaults to ``None``.
        parameter_name_prefix (``Category``, optional): Parameter name prefix for the ``ParameterValue``. Defaults to ``None``.
        si_prefix (``SiPrefix``, optional): SiPrefix associated with the ``ParameterValue``. Defaults to ``None``.
    """

    id: Optional[int] = None
    value: Optional[str] = None
    parameter_name_prefix: Optional[Category] = None
    si_prefix: Optional[SiPrefix] = None

    @staticmethod
    def from_dict(obj: dict) -> "ParameterValue":
        """
        Generate a ``ParameterValue`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ParameterValue``: The ``ParameterValue`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        value = from_union([from_str, from_none], obj.get("value"))
        parameter_name_prefix = from_union([Category.from_dict, from_none], obj.get("parameterNamePrefix"))
        si_prefix = from_union([SiPrefix.from_dict, from_none], obj.get("siPrefix"))
        return ParameterValue(id, value, parameter_name_prefix, si_prefix)

    def to_dict(self) -> dict:
        """
        Converts a ``ParameterValue`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``value``, ``parameterNamePrefix``, and ``siPrefix`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["value"] = from_union([from_str, from_none], self.value)
        result["parameterNamePrefix"] = from_union(
            [lambda x: to_class(Category, x), from_none],
            self.parameter_name_prefix,
        )
        result["siPrefix"] = from_union([lambda x: to_class(SiPrefix, x), from_none], self.si_prefix)
        return result


@dataclass
class Parameter:
    """
    A class to handle ``Parameter``.

    Args:
        id (int, optional): ``Parameter`` ID. Defaults to ``None``.
        parameter_model (``ParameterModel``, optional): ``ParameterModel`` of the ``Parameter``. Defaults to ``None``.
        parameter_values (list of ``ParameterValue``, optional): List of ``ParameterValue``. Defaults to ``None``.
    """

    id: Optional[int] = None
    parameter_model: Optional[ParameterModel] = None
    parameter_values: Optional[List[ParameterValue]] = None

    @staticmethod
    def from_dict(obj: dict) -> "Parameter":
        """
        Generate an ``Parameter`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Parameter``: The ``Parameter`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        parameter_model = from_union([ParameterModel.from_dict, from_none], obj.get("parameterModel"))
        parameter_values = from_union(
            [lambda x: from_list(ParameterValue.from_dict, x), from_none],
            obj.get("parameterValues"),
        )
        return Parameter(id, parameter_model, parameter_values)

    def to_dict(self) -> dict:
        """
        Converts a ``Parameter`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``parameterModel``, and ``parameterValues`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["parameterModel"] = from_union(
            [lambda x: to_class(ParameterModel, x), from_none],
            self.parameter_model,
        )
        result["parameterValues"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(ParameterValue, x), x),
                from_none,
            ],
            self.parameter_values,
        )
        return result


class DataStructure(Enum):
    ARRAY = "array"
    EMPTY = ""


class TypeEnum(Enum):
    EMPTY = ""
    FLOAT = "float"
    INT = "int"


@dataclass
class SensorModel:
    """
    A class to handle ``SensorModel``.

    Args:
        code (int, optional): Sensor code for the ``SensorModel``. Defaults to ``None``.
        id (int, optional): ``SensorModel`` ID. Defaults to ``None``.
        mapping (str, optional): Mapping of the ``SensorModel``. Defaults to ``None``.
        unit (``Category``, optional): Unit of the SensorModel. Defaults to ``None``.
        type (``TypeEnum``, optional): TypeEnum associated with the ``SensorModel``. Defaults to ``None``.
        data_structure (``DataStructure``, optional): DataStructure associated with the ``SensorModel``. Defaults to ``None``.
        scale (float, optional): Scale of the ``SensorModel``. Defaults to ``None``.
        is_virtual (bool, optional): Flag to indicate if ``SensorModel`` is virtual. Defaults to ``None``.
        virtual_expression (str, optional): If ``is_virtual`` is True, ``virtual_expression`` defines the expression use to evaluate the value of the ``SensorModel``. Defaults to ``None``.
    """

    code: Optional[int] = None  # ! Discrepancy: code is optional int here, but expects str or None in line 928.
    id: Optional[int] = None
    name: Optional[str] = None
    mapping: Optional[str] = None
    unit: Optional[str] = None
    type: Optional[TypeEnum] = None
    data_structure: Optional[DataStructure] = None
    scale: Optional[float] = None
    is_virtual: Optional[bool] = None
    virtual_expression: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "SensorModel":
        """
        Generate a ``SensorModel`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``SensorModel``: The ``SensorModel`` object.
        """
        assert isinstance(obj, dict)
        code = from_union([from_str, from_none], obj.get("code"))  # ! Discrepancy
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        mapping = from_union([from_str, from_none], obj.get("mapping"))
        unit = from_union([from_str, from_none], obj.get("unit"))
        type = from_union([TypeEnum, from_none], obj.get("type"))
        data_structure = from_union([DataStructure, from_none], obj.get("dataStructure"))
        scale = from_union([from_float, from_none], obj.get("scale"))
        is_virtual = from_union([from_bool, from_none], obj.get("isVirtual"))
        virtual_expression = from_union([from_str, from_none], obj.get("virtualExpression"))
        return SensorModel(
            code,
            id,
            name,
            mapping,
            unit,
            type,
            data_structure,
            scale,
            is_virtual,
            virtual_expression,
        )

    def to_dict(self) -> dict:
        """
        Converts a ``SensorModel`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``code``, ``id``, ``name``, ``mapping``, ``unit``, ``type``, ``dataStructure``, ``scale``, ``isVirtual`` and ``virtualExpression`` as keys.
        """
        result: dict = {}
        result["code"] = from_union(
            [
                lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x)),
            ],
            self.code,
        )
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["mapping"] = from_union([from_str, from_none], self.mapping)
        result["unit"] = from_union([from_str, from_none], self.unit)
        result["type"] = from_union([lambda x: to_enum(TypeEnum, x), from_none], self.type)
        result["dataStructure"] = from_union(
            [lambda x: to_enum(DataStructure, x), from_none],
            self.data_structure,
        )
        result["scale"] = from_union([to_float, from_none], self.scale)
        result["isVirtual"] = from_union([to_bool, from_none], self.is_virtual)
        result["virtualExpression"] = from_union([to_str, from_none], self.virtual_expression)
        return result


@dataclass
class Sensor:
    """
    A class to handle ``Sensor``.

    Args:
        name (str, optional): Name of the ``Sensor``. Defaults to ``None``.
        plotly_js (str, optional): Plotly JS string for the ``Sensor``. Defaults to ``None``.
        array_map (str, optional): Array Map of the ``SensorModel``. Defaults to ``None``.
        id (``Category``, optional): ``Sensor`` ID. Defaults to ``None``.
        format (``TypeEnum``, optional): Format of the ``Sensor``. Defaults to ``None``.
        sensor_model (``DataStructure``, optional): ``SensorModel`` associated with the Sensor. Defaults to ``None``.
        ranges (list, optional): Range of the ``Sensor``. Defaults to ``None``.
    """

    name: Optional[str] = None
    plotly_js: Optional[str] = None
    array_map: Optional[str] = None
    id: Optional[int] = None
    format: Optional[str] = None
    sensor_model: Optional[SensorModel] = None
    ranges: Optional[List[Any]] = None

    @staticmethod
    def from_dict(obj: dict) -> "Sensor":
        """
        Generate a ``Sensor`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Sensor``: The ``Sensor`` object.
        """
        assert isinstance(obj, dict)
        name = from_union([from_str, from_none], obj.get("name"))
        plotly_js = from_union([from_str, from_none], obj.get("plotlyJs"))
        array_map = from_union([from_str, from_none], obj.get("arrayMap"))
        id = from_union([from_int, from_none], obj.get("id"))
        format = from_union([from_none, from_str], obj.get("format"))
        sensor_model = from_union([SensorModel.from_dict, from_none], obj.get("sensorModel"))
        ranges = from_union([lambda x: from_list(lambda x: x, x), from_none], obj.get("ranges"))
        return Sensor(name, plotly_js, array_map, id, format, sensor_model, ranges)

    def to_dict(self) -> dict:
        """
        Converts a ``Sensor`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``name``, ``plotlyJs``, ``arrayMap``, ``id``, ``format``, ``sensorModel``, and ``ranges`` as keys.
        """
        result: dict = {}
        result["name"] = from_none(self.name)
        result["plotlyJs"] = from_none(self.plotly_js)
        result["arrayMap"] = from_union(
            [
                lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x)),
            ],
            self.array_map,
        )
        result["id"] = from_union([from_int, from_none], self.id)
        result["format"] = from_union([from_none, from_str], self.format)
        result["sensorModel"] = from_union([lambda x: to_class(SensorModel, x), from_none], self.sensor_model)
        result["ranges"] = from_union([lambda x: from_list(lambda x: x, x), from_none], self.ranges)
        return result


@dataclass
class GraphView:
    """
    A class to handle ``GraphView``.

    Args:
        graph_view_id (int, optional): ``GraphView`` ID. Defaults to ``None``.
        graph_view_name (str, optional): Name of the ``GraphView``. Defaults to ``None``.
        timespan (str, optional): Timespan of the ``GraphView``. Defaults to ``None``.
    """

    graph_view_id: Optional[int] = None
    graph_view_name: Optional[str] = None
    timespan: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "GraphView":
        """
        Generate a ``GraphView`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``GraphView``: The ``GraphView`` object.
        """
        assert isinstance(obj, dict)
        graph_view_id = from_union([from_none, lambda x: int(from_str(x))], obj.get("graphViewId"))
        graph_view_name = from_union([from_str, from_none], obj.get("graphViewName"))
        timespan = from_union([from_str, from_none], obj.get("timespan"))
        return GraphView(graph_view_id, graph_view_name, timespan)

    def to_dict(self) -> dict:
        """
        Converts a ``GraphView`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``graphViewId``, ``graphViewName``, and ``timespan`` as keys.
        """
        result: dict = {}
        result["graphViewId"] = from_union(
            [
                lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x)),
            ],
            self.graph_view_id,
        )
        result["graphViewName"] = from_union([from_str, from_none], self.graph_view_name)
        result["timespan"] = from_union([from_str, from_none], self.timespan)
        return result


@dataclass
class Indicator:
    """
    A class to handle ``Indicator``.

    Args:
        sensor_id (str, optional): ``Indicator`` ID. Defaults to ``None``.
        sensor_name (str, optional): Name of the ``Indicator``. Defaults to ``None``.
        indicator_type (str, optional): Type of the ``Indicator``. Defaults to ``None``.
    """

    sensor_id: Optional[str] = None
    sensor_name: Optional[str] = None
    indicator_type: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Indicator":
        """
        Generate an ``Indicator`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Indicator``: The ``Indicator`` object.
        """
        assert isinstance(obj, dict)
        sensor_id = from_union([from_str, from_none], obj.get("sensorId"))
        sensor_name = from_union([from_str, from_none], obj.get("sensorName"))
        indicator_type = from_union([from_str, from_none], obj.get("indicatorType"))
        return Indicator(sensor_id, sensor_name, indicator_type)

    def to_dict(self) -> dict:
        """
        Converts an ``Indicator`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``sensorId``, ``sensorName``, and ``indicatorType`` as keys.
        """
        result: dict = {}
        result["sensorId"] = from_union([from_str, from_none], self.sensor_id)
        result["sensorName"] = from_union([from_str, from_none], self.sensor_name)
        result["indicatorType"] = from_union([from_str, from_none], self.indicator_type)
        return result


@dataclass
class ViewTemplate:
    """
    A class to handle ``ViewTemplate``.

    Args:
        indicator (list of Indicator, optional): List of Indicator. Defaults to ``None``.
        graph_views (list of GraphView, optional): List of GraphView. Defaults to ``None``.
    """

    indicators: Optional[List[Indicator]] = None
    graph_views: Optional[List[GraphView]] = None

    @staticmethod
    def from_dict(obj: dict) -> "ViewTemplate":
        """
        Generate a ``ViewTemplate`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ViewTemplate``: The ``ViewTemplate`` object.
        """
        assert isinstance(obj, dict)
        indicators = from_union(
            [lambda x: from_list(Indicator.from_dict, x), from_none],
            obj.get("indicators"),
        )
        graph_views = from_union(
            [lambda x: from_list(GraphView.from_dict, x), from_none],
            obj.get("graphViews"),
        )
        return ViewTemplate(indicators, graph_views)

    def to_dict(self) -> dict:
        """
        Converts an ``ViewTemplate`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``indicators``, and ``graphViews`` as keys.
        """
        result: dict = {}
        result["indicators"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(Indicator, x), x),
                from_none,
            ],
            self.indicators,
        )
        result["graphViews"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(GraphView, x), x),
                from_none,
            ],
            self.graph_views,
        )
        return result


@dataclass
class ChildComponentModel:
    """
    A class to handle ``ChildComponentModel``.

    Args:
        id (int, optional): ``ChildComponentModel`` ID. Defaults to ``None``.
        quantity (int, optional): Quantity of ``ChildComponentModel``. Defaults to ``None``.
        is_virtual (bool, optional): Flag to indicate if the ``ChildComponentModel`` is virtual. Defaults to ``None``.
        model (Model, optional): ``Model`` for the ``ChildComponentModel``. Defaults to ``None``.
    """

    id: Optional[int] = None
    quantity: Optional[int] = None
    is_virtual: Optional[bool] = None
    model: Optional["Model"] = None

    @staticmethod
    def from_dict(obj: dict) -> "ChildComponentModel":
        """
        Generate a ``ChildComponentModel`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ChildComponentModel``: The ``ChildComponentModel`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        quantity = from_union([from_int, from_none], obj.get("quantity"))
        is_virtual = from_union([from_bool, from_none], obj.get("isVirtual"))
        model = from_union([Model.from_dict, from_none], obj.get("model"))
        return ChildComponentModel(id, quantity, is_virtual, model)

    def to_dict(self) -> dict:
        """
        Converts a ``ChildComponentModel`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``quantity``, ``isVirtual``, and ``model`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["quantity"] = from_union([from_int, from_none], self.quantity)
        result["isVirtual"] = from_union([from_bool, from_none], self.is_virtual)
        result["model"] = from_union([lambda x: to_class(Model, x), from_none], self.model)
        return result


@dataclass
class Model:
    """
    A class to handle ``Model``.

    Args:
        id (int, optional): ``Model`` ID. Defaults to ``None``.
        name (int, optional): Name of the ``Model``. Defaults to ``None``.
        is_configurable (bool, optional): Flag to indicate if the ``Model`` is configurable. Defaults to ``None``.
        configuration_type (str, optional): Type of configuration associated with the ``Model``. Defaults to ``None``.
        view_template (``ViewTemplate``, optional): ``ViewTemplate`` associated with the ``Model``. Defaults to ``None``.
        category (``Category``, optional): ``Category`` associated with the ``Model``. Defaults to ``None``.
        image (``Image``, optional): ``Image`` associated with the ``Model``. Defaults to ``None``.
        configurations (list of ``Configuration``, optional): List of ``Configuration``s associated with the ``Model``. Defaults to ``None``.
        parameters (list of ``Paramter``, optional): List of ``Parameter``s associated with the ``Model``. Defaults to ``None``.
        sensors (list of ``Sensor``, optional): List of ``Sensor``s associated with the ``Model``. Defaults to ``None``.
        child_component_models (list of ``ChildComponentModel``, optional): List of ``ChildComponentModel``s associated with the ``Model``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    is_configurable: Optional[bool] = None
    configuration_type: Optional[str] = None
    view_template: Optional[ViewTemplate] = None
    category: Optional[Category] = None
    image: Optional[Image] = None
    configurations: Optional[List[Configuration]] = None
    parameters: Optional[List[Parameter]] = None
    sensors: Optional[List[Sensor]] = None
    children_component_model_interfaces: List[ChildrenComponentModelInterface] = None
    child_component_models: Optional[List[ChildComponentModel]] = None

    @staticmethod
    def from_dict(obj: dict) -> "Model":
        """
        Generate a ``Model`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Model``: The ``Model`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        is_configurable = from_union([from_bool, from_none], obj.get("isConfigurable"))
        configuration_type = from_union([from_none, from_str], obj.get("configurationType"))
        view_template = from_union([ViewTemplate.from_dict, from_none], obj.get("viewTemplate"))
        category = from_union([Category.from_dict, from_none], obj.get("category"))
        image = from_union([Image.from_dict, from_none], obj.get("image"))
        configurations = from_union(
            [lambda x: from_list(Configuration.from_dict, x), from_none],
            obj.get("configurations"),
        )
        parameters = from_union(
            [lambda x: from_list(Parameter.from_dict, x), from_none],
            obj.get("parameters"),
        )
        sensors = from_union(
            [lambda x: from_list(Sensor.from_dict, x), from_none],
            obj.get("sensors"),
        )
        child_component_models = from_union(
            [lambda x: from_list(ChildComponentModel.from_dict, x), from_none],
            obj.get("childComponentModels"),
        ),
        children_component_model_interfaces = from_list(ChildrenComponentModelInterface.from_dict, obj.get("childrenComponentModelInterfaces"))

        return Model(
            id,
            name,
            is_configurable,
            configuration_type,
            view_template,
            category,
            image,
            configurations,
            parameters,
            sensors,
            child_component_models,
            children_component_model_interfaces

        )

    def to_dict(self) -> dict:
        """
        Converts a ``Model`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, ``isConfigurable``, ``configurationType``, ``viewTemplate``, ``category``, ``image``, ``configurations``, `parameters``, ``sensors`` and ``childComponentModels`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["isConfigurable"] = from_union([from_bool, from_none], self.is_configurable)
        result["configurationType"] = from_union([from_none, from_str], self.configuration_type)
        result["viewTemplate"] = from_union(
            [lambda x: to_class(ViewTemplate, x), from_none],
            self.view_template,
        )
        result["category"] = from_union([lambda x: to_class(Category, x), from_none], self.category)
        result["image"] = from_union([lambda x: to_class(Image, x), from_none], self.image)
        result["configurations"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(Configuration, x), x),
                from_none,
            ],
            self.configurations,
        )
        result["parameters"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(Parameter, x), x),
                from_none,
            ],
            self.parameters,
        )
        result["sensors"] = from_union(
            [lambda x: from_list(lambda x: to_class(Sensor, x), x), from_none],
            self.sensors,
        )
        result["childComponentModels"] = from_union(
            [
                lambda x: from_list(lambda x: to_class(ChildComponentModel, x), x),
                from_none,
            ],
            self.child_component_models,
        )
        result["childrenComponentModelInterfaces"] = from_list(lambda x: to_class(ChildrenComponentModelInterface, x), self.children_component_model_interfaces)
        return result


@dataclass
class Tag:
    """
    A class to handle ``Tag``.

    Args:
        id (int, optional): ``Tag`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Tag``. Defaults to ``None``.
        color (str, optional): Color of the ``Tag``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    color: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Tag":
        """
        Generate a ``Tag`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Tag``: The ``Tag`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        color = from_union([from_str, from_none], obj.get("color"))
        return Tag(id, name, color)

    def to_dict(self) -> dict:
        """
        Converts a ``Tag`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``color`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["color"] = from_union([from_str, from_none], self.color)
        return result


@dataclass
class EdisonGenericComponent:
    """
    A class to handle ``EdisonGenericComponent``.

    Args:
        id (int, optional): ``EdisonGenericComponent`` ID. Defaults to ``None``.
        serial_number (int, optional): Serial Number of ``EdisonGenericComponent``. Defaults to ``None``.
        iot_data (``IotData``, optional): ``IotData`` associated with the ``EdisonGenericComponent``. Defaults to ``None``.
        model (``Model``, optional): ``Model`` for the ``EdisonGenericComponent``. Defaults to ``None``.
        current_state (``CurrentState``, optional): ``CurrentState`` of the ``EdisonGenericComponent``. Defaults to None.
        tags (list of ``Tag``, optional): List of ``Tag``s associated with the ``EdisonGenericComponent``. Defaults to ``None``.
    """

    id: Optional[int] = None
    serial_number: Optional[str] = None
    iot_data: Optional[IotData] = None
    model: Optional[Model] = None
    current_state: Optional[CurrentState] = None
    tags: Optional[List[Tag]] = None
    df: Optional[DataFrame] = None
    dfList: Optional[List[DataFrame]] = None

    @staticmethod
    def from_dict(obj: dict) -> "EdisonGenericComponent":
        """
        Generate a ``EdisonGenericComponent`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``EdisonGenericComponent``: The ``EdisonGenericComponent`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        serial_number = from_union([from_str, from_none], obj.get("serialNumber"))
        iot_data = from_union([IotData.from_dict, from_none], obj.get("iotData"))
        model = from_union([Model.from_dict, from_none], obj.get("model"))
        current_state = from_union([CurrentState.from_dict, from_none], obj.get("currentState"))
        tags = from_union([lambda x: from_list(Tag.from_dict, x), from_none], obj.get("tags"))
        df = None
        dfList = []
        return EdisonGenericComponent(id, serial_number, iot_data, model, current_state, tags,df,dfList)

    def to_dict(self) -> dict:
        """
        Converts a ``EdisonGenericComponent`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``serialNumber``, ``iotData``, ``model``, ``currentState``, and ``tags`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["serialNumber"] = from_union([from_str, from_none], self.serial_number)
        result["iotData"] = from_union([lambda x: to_class(IotData, x), from_none], self.iot_data)
        result["model"] = from_union([lambda x: to_class(Model, x), from_none], self.model)
        result["currentState"] = from_union(
            [lambda x: to_class(CurrentState, x), from_none],
            self.current_state,
        )
        result["tags"] = from_union(
            [lambda x: from_list(lambda x: to_class(Tag, x), x), from_none],
            self.tags,
        )
        return result

@dataclass
class CurrentState:
    """
    A class to handle ``CurrentState``.

    Args:
        id (int, optional): ``CurrentState`` ID. Defaults to ``None``.
        location (str, optional): Location for the ``CurrentState``. Defaults to ``None``.
        description (str, optional): Description of the ``CurrentState``. Defaults to ``None``.
        current_state_datetime (``Datetime``, optional): ``Datetime`` for the ``CurrentState``. Defaults to ``None``.
        status (``Status``, optional): ``Status`` of the ``CurrentState``. Defaults to ``None``.
        status_existential (``Status``, optional): Existential ``Status`` of the ``CurrentState``. Defaults to ``None``.
        status_functional (``Status``, optional): Functional ``Status`` of the ``CurrentState``. Defaults to ``None``.
    """

    id: Optional[int] = None
    location: Optional[str] = None
    description: Optional[str] = None
    current_state_datetime: Optional[Datetime] = None
    status: Optional[Status] = None
    status_existential: Optional[Status] = None
    status_functional: Optional[Status] = None
    child_components:Optional[EdisonGenericComponent] = None
    @staticmethod
    def from_dict(obj: dict) -> "CurrentState":
        """
        Generate a ``CurrentState`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``CurrentState``: The ``CurrentState`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        location = from_union([from_str, from_none], obj.get("location"))
        description = from_union([from_str, from_none], obj.get("description"))
        current_state_datetime = from_union([Datetime.from_dict, from_none], obj.get("datetime"))
        status = from_union([Status.from_dict, from_none], obj.get("status"))
        status_existential = from_union([Status.from_dict, from_none], obj.get("statusExistential"))
        status_functional = from_union([Status.from_dict, from_none], obj.get("statusFunctional"))
        child_components = from_union([lambda x: from_list(EdisonGenericComponent.from_dict, x),from_none], obj.get('childComponents'))
        
        return CurrentState(
            id,
            location,
            description,
            current_state_datetime,
            status,
            status_existential,
            status_functional,
            child_components
        )

    def to_dict(self) -> dict:
        """
        Converts a ``CurrentStatue`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``location``, ``description``, ``datetime``, ``status``, ``statusExistental``, and ``statusFunctional`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["location"] = from_union([from_str, from_none], self.location)
        result["description"] = from_union([from_str, from_none], self.description)
        result["datetime"] = from_union(
            [lambda x: to_class(Datetime, x), from_none],
            self.current_state_datetime,
        )
        result["status"] = from_union([lambda x: to_class(Status, x), from_none], self.status)
        result["statusExistential"] = from_union([lambda x: to_class(Status, x), from_none], self.status_existential)
        result["statusFunctional"] = from_union([lambda x: to_class(Status, x), from_none], self.status_functional)
        return result



@dataclass
class Tag:
    """
    A class to handle ``Tag``.

    Args:
        id (int, optional): ``SiPrefix`` ID. Defaults to ``None``.
        description (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        name (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        color (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    description: Optional[str] = None
    color: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "Tag":
        """
        Generate an ``Tag`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Tag``: The ``Tag`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        description = from_union([from_str, from_none], obj.get("description"))
        color = from_union([from_str, from_none], obj.get("color"))
        return Tag(id, name, description, color)

    def to_dict(self) -> dict:
        """
        Converts an ``tag`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, and ``description``, and ``color`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["description"] = from_union([from_str, from_none], self.description)
        result["color"] = from_union([from_str, from_none], self.color)
        return result

@dataclass
class Dashboard:
    """
    A class to handle ``Dashboard``.

    Args:
        id (int, optional): ``SiPrefix`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        tags (list of ``Tag``, optional): List of ``Tag``s associated with the ``EdisonGenericComponent``. Defaults to ``None``.
        projectId (int, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        iconClass (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        iconBgColor (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        targetedTemplateId (int, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        accessibility (str, optional): Name of the ``SiPrefix``. Defaults to ``None``.
        isTemplate (bool, optional): Name of the ``SiPrefix``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    tags: Optional[List[Tag]] = None
    projectId: Optional[int] = None
    iconClass: Optional[str] = None
    iconBgColor: Optional[str] = None
    targetedTemplateId: Optional[int] = None
    accessibility: Optional[str] = None
    isTemplate: Optional[bool] = None

    @staticmethod
    def from_dict(obj: dict) -> "Dashboard":
        """
        Generate an ``Dashboard`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Dashboard``: The ``Dashboard`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        tags = from_union([lambda x: from_list(Tag.from_dict, x), from_none], obj.get("tags"))
        projectId = from_union([from_int, from_none], obj.get("projectId"))
        iconClass = from_union([from_str, from_none], obj.get("iconClass"))
        iconBgColor = from_union([from_str, from_none], obj.get("iconBgColor"))
        targetedTemplateId = from_union([from_int, from_none], obj.get("targetedTemplateId"))
        accessibility = from_union([from_str, from_none], obj.get("accessibility"))
        isTemplate = from_union([from_bool, from_none], obj.get("isTemplate"))
        return Dashboard(id, name, tags, projectId, iconClass, iconBgColor, targetedTemplateId, accessibility, isTemplate)

    def to_dict(self) -> dict:
        """
        Converts an ``Dashboard`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, ``tags``, ``projectId``, ``iconClass``, ``iconBgColor``, ``targetedTemplateId``, ``accessibility``, and ``isTemplate`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["tags"] = from_union(
            [lambda x: from_list(lambda x: to_class(Tag, x), x), from_none],
            self.tags,
        )
        result["projectId"] = from_union([from_int, from_none], self.projectId)
        result["iconClass"] = from_union([from_str, from_none], self.iconClass)
        result["iconBgColor"] = from_union([from_str, from_none], self.iconBgColor)
        result["targetedTemplateId"] = from_union([from_int, from_none], self.targetedTemplateId)
        result["accessibility"] = from_union([from_str, from_none], self.accessibility)
        result["isTemplate"] = from_union([from_bool, from_none], self.isTemplate)
        return result

@dataclass
class ActivityMetadata:
    """
    A class to handle ``ActivityMetadata``.

    Args:
        id (int, optional): ``ActivityMetadata`` ID. Defaults to ``None``.
        metadataKey (str, optional): key of the ``ActivityMetadata``. Defaults to ``None``.
        metadataValue (str, optional): value of the ``ActivityMetadata``. Defaults to ``None``.
    """

    id: Optional[int] = None
    metadataKey: Optional[str] = None
    metadataValue: Optional[str] = None

    @staticmethod
    def from_dict(obj: dict) -> "ActivityMetadata":
        """
        Generate an ``ActivityMetadata`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``ActivityMetadata``: The ``ActivityMetadata`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        metadataKey = from_union([from_str, from_none], obj.get("metadataKey"))
        metadataValue = from_union([from_str, from_none], obj.get("metadataValue"))

        return ActivityMetadata(id, metadataKey, metadataValue)

    def to_dict(self) -> dict:
        """
        Converts an ``ActivityMetadata`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``metadataKey``, ``metadataValue`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["metadataKey"] = from_union([from_str, from_none], self.metadataKey)
        result["metadataValue"] = from_union([from_str, from_none], self.metadataValue)
        return result




@dataclass
class Activity:
    """
    A class to handle ``Activity``.

    Args:
        id (int, optional): ``Activity`` ID. Defaults to ``None``.
        name (str, optional): Name of the ``Activity``. Defaults to ``None``.
        description (str, optional): Description of the ``Activity``. Defaults to ``None``.
        location (str, optional): Location of the ``Activity``. Defaults to ``None``.
        tags (list of ``Tag``, optional): List of ``Tag``s associated with the ``Activity``. Defaults to ``None``.
        assets (list of ``Asset``, optional): List of ``Asset``s associated with the ``Activity``. Defaults to ``None``.
        startDate (``Datetime``, optional): ``Datetime`` start date for the ``Activity``. Defaults to ``None``.
        endDate (``Datetime``, optional): ``Datetime`` end date for the ``Activity``. Defaults to ``None``.
    """

    id: Optional[int] = None
    name: Optional[str] = None
    description: Optional[str] = None
    location: Optional[str] = None
    metadata: Optional[List[ActivityMetadata]] = None
    tags: Optional[List[Tag]] = None
    assets: Optional[List[EdisonGenericComponent]] = None
    startDate: Optional[float] = None
    endDate: Optional[float] = None

    @staticmethod
    def from_dict(obj: dict) -> "Activity":
        """
        Generate an ``Activity`` object from a dictionary.

        Args:
            obj (dict): Dictionary containing the keys as the arguments to create the object.

        Returns:
            ``Activity``: The ``Activity`` object.
        """
        assert isinstance(obj, dict)
        id = from_union([from_int, from_none], obj.get("id"))
        name = from_union([from_str, from_none], obj.get("name"))
        description = from_union([from_str, from_none], obj.get("description"))
        location = from_union([from_str, from_none], obj.get("location"))
        metadata = from_union([lambda x: from_list(ActivityMetadata.from_dict, x), from_none], obj.get("activityMetadatas"))
        tags = from_union([lambda x: from_list(Tag.from_dict, x), from_none], obj.get("tags"))
        assets = from_union([lambda x: from_list(EdisonGenericComponent.from_dict, x), from_none], obj.get("assets"))
        startDate = from_union([from_float, from_none], obj.get("startDate"))
        endDate = from_union([from_float, from_none], obj.get("endDate"))

        return Activity(id, name, description, location, metadata, tags, assets, startDate, endDate)

    def to_dict(self) -> dict:
        """
        Converts an ``Activity`` object into a dictionary.

        Returns:
            dict: Dictionary containing ``id``, ``name``, ``description``, ``location``, ``tags``, ``assets``, ``startDate``, ``endDate`` as keys.
        """
        result: dict = {}
        result["id"] = from_union([from_int, from_none], self.id)
        result["name"] = from_union([from_str, from_none], self.name)
        result["description"] = from_union([from_str, from_none], self.description)
        result["location"] = from_union([from_str, from_none], self.location)
        result["activityMetadatas"] = from_union(
            [lambda x: from_list(lambda x: to_class(ActivityMetadata, x), x), from_none],
            self.metadata,
        )
        result["tags"] = from_union(
            [lambda x: from_list(lambda x: to_class(Tag, x), x), from_none],
            self.tags,
        )
        result["assets"] = from_union(
            [lambda x: from_list(lambda x: to_class(EdisonGenericComponent, x), x), from_none],
            self.assets,
        )
        result["startDate"] = from_union([from_float, from_none], self.startDate)
        result["endDate"] = from_union([from_float, from_none], self.endDate)

        return result


def edison_generic_component_from_dict(s: dict) -> EdisonGenericComponent:
    """
    Create an ``EdisonGenericComponent`` from a dictionary with the correct keys as arguments.

    Args:
        s (dict): [description]

    Returns:
        ``EdisonGenericComponent``: An ``EdisonGenericComponent`` object.
    """
    return EdisonGenericComponent.from_dict(s)


def edison_generic_component_to_dict(x: EdisonGenericComponent) -> dict:
    """[summary]

    Args:
        x (``EdisonGenericComponent``): ``EdisonGenericComponent`` to convert to dictionary.

    Returns:
        dict: Dictionary containing ``id``, ``serialNumber``, ``iotData``, ``model``, ``currentState``, and ``tags`` as keys.
    """
    return to_class(EdisonGenericComponent, x)
