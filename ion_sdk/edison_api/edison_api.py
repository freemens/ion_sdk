import json
import os
import time
from typing import List
from collections.abc import Iterable
from time import sleep
import random
import string
import warnings
from uplink import (
    Consumer,
    get,
    Path,
    Query,
    headers,
    returns,
    Body,
    post,
    multipart,
    Part,
    Field,
    retry,
    PartMap,
    FieldMap,
    error_handler,
    response_handler,
    ratelimit,
)
from uplink.retry.stop import after_attempt
from uplink.retry.backoff import fixed
from uplink.retry.when import raises, status
from ion_sdk.edison_api.models.iotModel import (
    sensor_data_from_dict,
)  # Imported but unused

from ion_sdk.edison_api.models.factory import (
    getSensorNameByCode,
    getSensorByName,
    getSensorByCode,
)

from ion_sdk.edison_api.models.factoryModel import (
    Activity,
    Dashboard,
    Model,
    CurrentState,    
    EdisonGenericComponent,
    Tag
)
from math import isnan
from fuzzywuzzy import process
from datetime import datetime
import pandas as pd
from ion_sdk.tools.utils import progress
import getpass
from enum import Enum


def raiseApiResponse(response):
    pass


def raiseApiError(exc_type, exc_val, exc_tb):
    pass  # print(exc_type)


urlAddComponent = "core/api/v2/factory/components/add"
urlChangeComponentState = "core/api/v2/factory/component/{serialNumber}/change-state"
urlDeleteComponent = "core/api/v2/factory/component/{serialNumber}/remove"
urlAssembleComponent = "core/api/v2/factory/component/assembly"


class EdisonConsumer(Consumer):
    def __init__(self, edisonDomainPath, apiKey):
        Consumer.__init__(self, edisonDomainPath)
        self.session.headers["api-key"] = apiKey

    @returns.json()
    @get("/core/api/v2/iot/task/{taskId}")
    def getTaskById(self, taskId):
        pass

    @get("core/iot/tasks")
    def getTask(self):
        pass

    @returns.json()
    @get("/core/api/v2/activities?term={name}")
    def getActivities(self, name: str):
        pass

    @post("/core/api/v2/activities/add")
    def createActivity(self, payload: Body):
        pass

    @post("/core/api/v2/activities/{activityId}/remove")
    def removeActivity(self, activityId):
        pass

    @post("/core/api/v2/activities/{activityId}/edit")
    def editActivity(self, activityId, payload: Body):
        pass

    @returns.json()
    @get("/core/api/v2/tags?term={name}")
    def getTags(self, name: str):
        pass

    @returns.json()
    @post("/core/api/v2/tags/link-components")
    def linkTagToAssets(self, payload: Body):
        pass

    @returns.json()
    @get("/core/api/v2/tags/{tagId}")
    def getTag(self, tagId):
        pass

    @post("/core/api/v2/tags/add")
    def createTag(self, payload: Body):
        pass

    @post("/core/api/v2/tags/{tagId}/remove")
    def removeTag(self, tagId):
        pass

    @post("/core/api/v2/tags/{tagId}/edit")
    def editTag(self, tagId, payload: Body):
        pass

    @returns.json()
    @get("/core/api/v2/iot/dashboards/{dashboardId}")
    def getDashboard(self, dashboardId):
        pass

    @get("core/api/v2/iot/dashboards?name={name}")
    def getDashboards(self, name):
        pass

    @post("/core/api/v2/iot/dashboards/add")
    def createDashboard(self, payload: Body):
        pass

    @post("/core/api/v2/iot/dashboards/{dashboardId}/remove")
    def removeDashboard(self, dashboardId):
        pass

    @post("/core/api/v2/iot/dashboards/{dashboardId}/edit")
    def editDashboard(self, dashboardId, payload: Body):
        pass

    @returns.json()
    @get("core/api/v2/factory/components?start=0&length=200&filters[0][attribute]=serialNumber&filters[0][value]={serialNumber}")
    def getComponent(self, serialNumber):
        pass

    @returns.json()
    @get("core/api/v2/factory/components?start=0&length={maxRecords}&filters[0][attribute]={filterAttribute}&filters[0][value]={filterValue}")
    def getComponentByFilter(self, maxRecords, filterAttribute, filterValue):
        pass

    @returns.json()
    @get("core/api/v2/factory/components?start=0&length={maxRecords}&filters[0][attribute]=serialNumber&filters[0][value]={serialNumber}")
    def getAllComponents(self, serialNumber, maxRecords):
        pass

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/data/task/sentinel/soh/terminate")
    def terminateSohTask(self, task_info: Body):
        pass

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/data/task/update-info")
    def updateTaskInfo(self, task_info: Body):
        pass

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/data/direct-insert")
    def sendSensorData(self, sensor_info: Body):
        pass

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/raw-insight")
    def getSensorData(self, sensor_info: Body):
        pass    

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/insight")
    def getAggregatedSensorData(self, sensor_info: Body):
        pass

    @ratelimit(calls=1, period=10)
    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/data/erase")
    def deleteSensorData(self, sensor_info: Body):
        pass

    @multipart
    @error_handler(raiseApiError)
    @retry(
        when=raises(Exception) | status(400),
        backoff=fixed(1),
        stop=after_attempt(3),
    )
    @returns.json()
    @post("api/v1/data/file-upload")
    # @post("/api/v1/data/signed-upload-url")
    def uploadFile(self, files: PartMap, data: FieldMap):
        pass

    @returns.json()
    @post("/api/v1/datasets")
    # @post("/api/v1/data/signed-upload-url")
    def uploadDatasetToAsset(self, files: PartMap, data: FieldMap):
        pass

    @post(urlAddComponent)
    @returns.json()
    def addComponent(self, component_info: Body):
        pass

    @post(urlChangeComponentState)
    @returns.json()
    def changeComponentState(self, serialNumber, component_info: Body):
        pass

    @post(urlDeleteComponent)
    @returns.json()
    def removeComponent(self, serialNumber):
        pass

    @ratelimit(calls=1, period=10)
    @headers({"Content-Type": "application/json"})
    @post("api/v1/data/erase-all")
    @returns.json()
    def eraseAllData(self, component_info: Body):
        pass

    @headers(
        {
            "Content-Type": "application/json",
            "auth-token": "4780132a-36e4-4627-9d3c-ca1afa712fe6",
        }
    )
    @returns.json()
    @post("api/v1/data/update-component-sentinel-parameters")
    def updateComponentSentinelParameters(self, componentSentinelParameters: Body):
        pass

    @headers(
        {
            "Content-Type": "application/json"
        }
    )
    @returns.json()
    @post(urlAssembleComponent)
    def assembleComponents(self, componentAssemblyParameters: Body):
        pass


class dataUpdateMethod(Enum):
    REPLACE = "replace"
    INSERT = "insert"
    APPEND = "append"


class Client:
    """
    API Client for data-exhange between python application and Edison platform.
    """

    def __init__(
        self,
        apiKey: str,
        factory_api: str = "https://edison.ionenergy.co" ,
        iot_api: str = "https://iot.edison.ionenergy.co" ,
        basicAuthKey: str = "none",
    ):
        """
        Initialise the Client object using the API Key provided by the user.

        Args:
            apiKey (str): API Key of the Client.
            factory_api (str): URL to your desired factory API URL ("https://edison.ionenergy.co" by default)
            iot_api (str): URL to your desired iot API URL ("https://iot.edison.ionenergy.co"" by default)
            basicAuthKey (str, optional): To-do. Defaults to "none".
        """
        default_factory_api = "https://edison.ionenergy.co"
        default_iot_api = "https://iot.edison.ionenergy.co"
        env_factory_api = os.environ.get("ALTERGO_FACTORY_API", None)
        env_iot_api = os.environ.get("ALTERGO_IOT_API", None)

        selected_factory_api = factory_api
        selected_iot_api = iot_api

        isDefaultApiEndpoints = True
        envVariableDefined = False

        if env_factory_api is not None or env_iot_api is not None:
            envVariableDefined = True

        if not (default_factory_api == factory_api and iot_api == default_iot_api):
            isDefaultApiEndpoints = False

        if isDefaultApiEndpoints:
            if envVariableDefined:
                selected_factory_api = env_factory_api
                selected_iot_api = env_iot_api
            else:
                if env_factory_api is None:
                    warnings.warn(
                        """Could not find Factory API Endpoints in Environment Variables. Using Edison Defaults instead.\n
                        To provide API Endpoints in the Environment Variables, refer to https://ion-energy.gitlab.io/edison-kb-private/docs/edison/library/ion-sdk/installation""",
                        UserWarning,
                    )

                if env_iot_api is None:
                    warnings.warn(
                        """Could not find IOT API Endpoints in Environment Variables. Using Edison Defaults instead.\n
                        To provide API Endpoints in the Environment Variables, refer to https://ion-energy.gitlab.io/edison-kb-private/docs/edison/library/ion-sdk/installation""",
                        UserWarning,
                    )
        else:
            warnings.warn(
                "Parameters have been given for Endpoints... overriding Environements variables",
                UserWarning
            )        
        self.domain = selected_factory_api
        self.factoryApi = EdisonConsumer(selected_factory_api, apiKey)
        self.iotApi = EdisonConsumer(selected_iot_api, apiKey)

        # localhost
        # self.factoryApi=EdisonConsumer("http://localhost:8000/",apiKey)
        # localhost
        # self.iotApi=EdisonConsumer("http://iot.localhost:3000/",apiKey)

    def getAsset(self, serialNumber: str) -> EdisonGenericComponent:
        """
        Get ``assets`` by serial number from the Edison platform.

        Args:
            serialNumber (str): Serial Number of the entity.

        Returns:
            EdisonGenericComponent: An ``asset`` that is defined within Edison.
            ``None`` is returned if the serial-number does not correspond to
            any ``asset`` registered with the platform.
        """

        req = self.factoryApi.getComponent(serialNumber)

        ediComp = None
        if len(req["data"]) > 0:
            component = req["data"][0]
            ediComp = EdisonGenericComponent.from_dict(component)

        return ediComp

    def getActivitiesByName(self, name: str):
        """
        Get ``activities`` by activity name from the Altergo platform.

        Args:
            activityName (str): name of the activity.

        Returns:
            List[Activities]: A list of Activities. Empty array is returned if no activity has been found.
        """

        req = self.factoryApi.getActivities(name)
        activities = []
        for act in req:
            act["startDate"] = float(act["startDate"])
            act["endDate"] = float(act["endDate"])
            if act["name"] == name:
                activities.append(Activity.from_dict(act))
        return activities

    def getActivityById(self, activityId: int):

        """
        Get ``activities`` by activity ID from the Altergo platform.

        Args:
            activityId (int): name of the activity.

        Returns:
            Activity: an activity. None is returned if no activity has been found.
        """

        req = self.factoryApi.getActivities("todo")
        for act in req:
            act["startDate"] = float(act["startDate"])
            act["endDate"] = float(act["endDate"])
            if act["id"] == activityId:
                return Activity.from_dict(act)
        return None

    def getAllActivities(self):
        """
        Get all ``activities``  from the Altergo platform.

        Args:

        Returns:
            List[Activities]: A list of Activities. Empty array is returned if no tag has been found.
        """

        req = self.factoryApi.getActivities("todo")
        activities = []
        for act in req:
            act["startDate"] = float(act["startDate"])
            act["endDate"] = float(act["endDate"])
            activities.append(Activity.from_dict(act))

        return activities

    def createActivity(self, activity: Activity) -> dict:
        """
        Create a new ``activity``.

        Args:
            activity (Activity): activity object that will be created.

        Returns:
            Activity: The created activity with its Altergo ID.
        """

        tagNames = []
        for tag in activity.tags:
            tagNames.append(tag.name)

        assetsSns = []
        for asset in activity.assets:
            assetsSns.append(asset.serial_number)

        metadata = []
        for md in activity.metadata:
            metadata.append({
                "metadataKey" : md.metadataKey,
                "metadataValue" : md.metadataValue
            })

        payload = {
            "name": activity.name,
            "description": activity.description,
            "location": activity.location,
            "metadata": metadata,
            "tags": tagNames,
            "assets": assetsSns,
            "startDate": int(activity.startDate) ,
            "endDate": int(activity.endDate),
        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.createActivity(jsonBody)

        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Activity.from_dict(jsonResp["data"])
        return None

    def editActivity(self, activity: Activity) -> dict:
        """
        Edit a ``activity``.

        Args:
            activity (Activity): activity object that will be edited.

        Returns:
            Activity: the edited activity.
        """
        tagNames = []
        for tag in activity.tags:
            tagNames.append(tag.name)

        assetsSns = []
        for asset in activity.assets:
            assetsSns.append(asset.serial_number)

        metadata = []
        for md in activity.metadata:
            metadata.append({
                "metadataKey" : md.metadataKey,
                "metadataValue" : md.metadataValue
            })

        payload = {
            "name": activity.name,
            "description": activity.description,
            "location": activity.location,
            "metadata": metadata,
            "tags": tagNames,
            "assets": assetsSns,
            "startDate": activity.startDate,
            "endDate": activity.endDate,

        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.editActivity(activity.id, jsonBody)

        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Activity.from_dict(jsonResp["data"])

        return None

    def removeActivity(self, activity: Activity) -> dict:
        """
        Remove an ``activity``.

        Args:
            activity (Activity): activity object that will be removed.

        Returns:
            serverResponse: The server response. Code 200 means that the activity has been removed.
        """
        resp = self.factoryApi.removeActivity(activity.id)

        jsonResp = json.loads(resp.text)
        return jsonResp

    def getTagsByName(self, name: str):
        """
        Get ``tags`` by tag name from the Altergo platform.

        Args:
            tagName (str): name of the tag.

        Returns:
            List[Tags]: A list of Tags. Empty array is returned if no tag has been found.
        """

        req = self.factoryApi.getTags(name)
        tags = []
        for tag in req:
            tags.append(Tag.from_dict(tag))
        return tags

    def linkTagToAssets(self, tag: Tag, assets: List[EdisonGenericComponent]) -> dict:
        """
        Link ``Tag`` to assets

        Args:
            tag (Tag): the tag
            List[EdisonGenericComponent]: the list of assets

        Returns:
            serverResponse: The server response. Code 200 means that the link has been established.
        """
        assetsIds = []
        for asset in assets:
            assetsIds.append(asset.id)

        payload = {
            "tag": {
                "text" : tag.name
            },
            "componentIds": assetsIds
        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.linkTagToAssets(jsonBody)
        return resp

    def createTag(self, tag: Tag) -> dict:
        """
        Create a new ``tag``.

        Args:
            tag (Tag): tag object that will be created.

        Returns:
            Tag: The created tag with its Altergo ID.
        """
        payload = {
            "name": tag.name,
            "color": tag.color,
            "description": tag.description
        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.createTag(jsonBody)
        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Tag.from_dict(jsonResp["data"])

        return None

    def removeTag(self, tag: Tag) -> dict:
        """
        Remove a ``tag``.

        Args:
            tag (Tag): tag object that will be removed.

        Returns:
            serverResponse: The server response. Code 200 means that the tag has been removed.
        """
        resp = self.factoryApi.removeTag(tag.id)

        jsonResp = json.loads(resp.text)
        return jsonResp

    def editTag(self, tag: Tag, payload: dict) -> dict:
        """
        Edit a ``tag``.

        Args:
            tag (Tag): tag object that will be edited.
            payload (Dict): the payload that contains the new values for targeted fields.

        Returns:
            Tag: the edited tag.
        """
        jsonBody = json.dumps(payload)
        resp = self.factoryApi.editTag(tag.id, jsonBody)
        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Tag.from_dict(jsonResp["data"])

        return None

    def getDashboardsByName(self, dashboardName: str):
        """
        Get ``dashboards`` by name from the Altergo platform.

        Args:
            dashboardName (str): name of the dashboard.

        Returns:
            List[Dashboards]: A list of Dashboards. Empty array is returned if no dashboard has been found.
        """
        resp = self.factoryApi.getDashboards(dashboardName)

        if resp.status_code == 200:
            dashboards = []
            jsonResp = json.loads(resp.text)
            for dash in jsonResp["data"]:
                dashboards.append(Dashboard.from_dict(dash))
            return dashboards
        return None

    def createDashboard(self, dashboard: Dashboard) -> dict:
        """
        Create a new ``dashboard``.

        Args:
            dashboard (Dashboard): dashboard object that will be created.

        Returns:
            Dashboard: The created dashboard with its Altergo ID.
        """
        tagNames = []
        for tag in dashboard.tags:
            tagNames.append(tag.name)

        payload = {
            "name": dashboard.name,
            "tags": tagNames,
            "projectId": dashboard.projectId,
            "iconClass": dashboard.iconClass,
            "iconBgColor": dashboard.iconBgColor,
            "targetedTemplateId": dashboard.targetedTemplateId,
            "accessibility": dashboard.accessibility,
            "isTemplate": dashboard.isTemplate
        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.createDashboard(jsonBody)

        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Dashboard.from_dict(jsonResp["data"])

        return None

    def removeDashboard(self, dashboard: Dashboard) -> dict:
        """
        Remove a ``dashboard``.

        Args:
            dashboard (Dashboard): dashboard object that will be removed.

        Returns:
            serverResponse: The server response. Code 200 means that the dashboard has been removed.
        """

        resp = self.factoryApi.removeDashboard(dashboard.id)
        jsonResp = json.loads(resp.text)
        return jsonResp

    def editDashboard(self, dashboard: Dashboard) -> dict:
        """
        Edit a ``dashboard``.
        Args:
            dashboard (Dashboard): dashboard object that will be edited.
        Returns:
            Dashboard: the edited dashboard.
        """
        tagNames = []
        for tag in dashboard.tags:
            tagNames.append(tag.name)

        payload = {
            "name": dashboard.name,
            "tags": tagNames,
            "projectId": dashboard.projectId,
            "iconClass": dashboard.iconClass,
            "iconBgColor": dashboard.iconBgColor,
            "targetedTemplateId": dashboard.targetedTemplateId,
            "accessibility": dashboard.accessibility,
            "isTemplate": dashboard.isTemplate
        }

        jsonBody = json.dumps(payload)
        resp = self.factoryApi.editDashboard(dashboard.id, jsonBody)

        if resp.status_code == 200:
            jsonResp = json.loads(resp.text)
            return Dashboard.from_dict(jsonResp["data"])

        return None

    def assembleAsset(self, parentComponentId: int, childrenComponentsIds: List[int]) -> string:

        body = {}
        body["componentId"] = parentComponentId
        body["childrenAssemble"] = []
        body["childrenDisassemble"] = []

        for childId in childrenComponentsIds:
            body["childrenAssemble"].append({"childId" : childId})

        jsonBody = json.dumps(body)
        resp = self.factoryApi.assembleComponents(jsonBody)
        return resp

        pass

    def getAssetsByFilter(self, filterAttribute: str, filterValue: str, maxRecords=200) -> EdisonGenericComponent:
        """
        Get ``assets`` by serial number from the Edison platform.

        Args:
            filterAttribute (str): Name of the EdisonGenericComponent attribute
            filterValue (str): Value of the attribute
             maxRecords (int): Maximum number of records. (we recommend no more than 200 at the time.)
        Returns:
            EdisonGenericComponent: An ``asset`` that is defined within Edison.
            ``None`` is returned if the serial-number does not correspond to
            any ``asset`` registered with the platform.
        """

        req = self.factoryApi.getComponentByFilter(maxRecords, filterAttribute, filterValue)
        assets = []
        if len(req["data"]) > 0:
            for comp in req['data']:
                assets.append(EdisonGenericComponent.from_dict(comp))
        return assets

    def getAssets(self, serialNumber, maxRecords) -> EdisonGenericComponent:
        """
        Get all ``assets`` by serialNumber. partial SerialNumbers will retrieve all assets containing substring.

        Args:
            serialNumber (str): Serial Number of the entity. or substring of a serialNumber
            maxRecords (int): Maximum number of records. (we recommend no more than 200 at the time.)

        Returns:
            [EdisonGenericComponents]: An array of ``asset`` that is defined within Edison.
            [] is returned if no asset are presents.            
        """

        complete = False
        assets = []

        while not complete:
            req = self.factoryApi.getAllComponents(serialNumber, maxRecords)
            for comp in req['data']:
                assets.append(EdisonGenericComponent.from_dict(comp))
            if len(req['data']) <= maxRecords:
                complete = True
        return assets

    def getAssetDataFrame(
        self,
        assets: list,
        sensorNames: list,
        startDate: int,
        endDate: int,
        raw: bool = True,
        targetedValue: string = "val",
        **kwargs
    ) -> pd.DataFrame:
        """
        Loads a Time Indexed [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) into each ``asset`` in
        the list ``assets``.

        Args:
            assets (list): List of EdisonGenericComponent. These components must have the same model.
            sensorNames (list): List containing sensor names as strings.
            startDate (float): POSIX timestamp in milliseconds, start
                bracket for sensor data range.
            endDate (float): POSIX timestamp in milliseconds, end bracket
                for sensor data range.
            raw (bool): if False, will return non aggregated data from sensors.

        Returns:
            pandas.DataFrame: [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) containing time
            indexed values for sensors specified.
            ``None`` is returned if the serial-number does not correspond to
            any ``asset`` registered with the platform.

        Note:
            The method, if executed correctly, will return an empty DataFrame. However, the assets within
            ``assets`` will have an attribute ``df`` containing the
            [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html).

        Warning:
            Do not assign the output of this method to the ``df`` attribute of the ``asset``. This will overwrite the dataframe with an empty dataframe.

        Raises:
            ValueError if no data is available in the time between ``startDate`` and ``endDate``.
        """

        # Creating a list of dicts which contains information about the sensor ID and the sensor position

        valid_assets = [True if asset.model == assets[0].model else False for asset in assets]
        if not all(valid_assets):
            raise ValueError("Assets provided do not have the same model. Please verify the list of assets.")

        sensorIdList = []
        for s in sensorNames:
            sensor = getSensorByName(assets[0].model, s)
            if sensor is None:
                raise KeyError(f"Sensor {s} is not a valid sensor associated with the model {assets[0].model.name} of asset {assets[0].serial_number}")
            else:
                for i in range(0, int(sensor.array_map)):
                    sensorId = {}
                    sensorId["sensorId"] = sensor.sensor_model.code
                    sensorId["sensorPosition"] = str(i)
                    sensorIdList.append(sensorId)

        # Creating a list of serial numbers
        serialNumbers = []
        for comp in assets:
            serialNumbers.append(comp.serial_number)

        # Creating a data request dict
        sensorDatarequest = {
            "serialNumber": serialNumbers,
            "sensorIds": sensorIdList,
            "startDate": startDate,
            "endDate": endDate,
        }

        dates = [startDate]
        timeChunk = 3600 * 24 * 1 * 1000 / len(serialNumbers)

        i = timeChunk
        while (startDate + i) < endDate:
            dates.append(startDate + i)
            i = i + timeChunk
        dates.append(endDate)

        time = []
        value = []
        sdf = pd.DataFrame()
        prettyStart = datetime.utcfromtimestamp(startDate / 1000).strftime("%Y-%m-%d %H:%M:%S UTC")
        prettyEnd = datetime.utcfromtimestamp(int(endDate / 1000)).strftime("%Y-%m-%d %H:%M:%S UTC")

        print("getting sensors: %s from: %s to : %s" % ("-".join(sensorNames), prettyStart, prettyEnd))

        for i in range(0, len(dates) - 1):
            sensorDatarequest = {
                "serialNumber": serialNumbers,
                "sensorIds": sensorIdList,
                "startDate": dates[i],
                "endDate": dates[i + 1],
            }

            progress(int((i + 1) / (len(dates) - 1) * 100))
            body = json.dumps(sensorDatarequest)      
            if raw:
                localResponse = self.iotApi.getSensorData(body)
                targetedValue = "val"
            else:
                localResponse = self.iotApi.getAggregatedSensorData(body)
                if targetedValue == "val":
                    targetedValue = 'avg'

            for j in localResponse:
                if len(j["data"]) == 0:
                    print(
                        f"\nAsset with Serial Number {j['serialNumber']} does not contain data on sensors {*sensorNames,} between {datetime.utcfromtimestamp(dates[i] // 1000).strftime('%Y-%m-%d %H:%M:%S UTC')} and {datetime.utcfromtimestamp(dates[i+1] // 1000).strftime('%Y-%m-%d %H:%M:%S UTC')}."
                    )
                else:
                    for sensor in j["data"]:
                        for index, record in enumerate(sensor["sensorRecords"]):  # shady enumerate
                            try:
                                time.append(datetime.utcfromtimestamp((record["ts"]) / 1000.0))
                            except TypeError:
                                time.append(datetime.strptime(record["ts"], "%Y-%m-%dT%H:%M:%S.%fZ"))

                            value.append(record["val"])
                        tempSensor = getSensorByCode(assets[0].model, sensor["_id"]["sensorId"])
                        if tempSensor.array_map == "1":
                            columnName = tempSensor.sensor_model.name
                        else:
                            columnName = tempSensor.sensor_model.name + "|" + sensor["_id"]["sensorPosition"]
                        df = pd.DataFrame(time, columns=["date"])
                        df[columnName] = value
                        df = df.drop_duplicates("date")
                        df.set_index("date", inplace=True)

                        for index, comp in enumerate(assets):
                            if j["serialNumber"] == assets[index].serial_number:
                                assets[index].dfList.append(df)
                        time.clear()
                        value.clear()

        for comp in assets:
            if len(comp.dfList) == 0:
                raise ValueError(
                    f"No data present in asset with serial number {comp.serial_number} on sensors {*sensorNames,} between {datetime.utcfromtimestamp(startDate // 1000).strftime('%Y-%m-%d %H:%M:%S UTC')} and {datetime.utcfromtimestamp(endDate // 1000).strftime('%Y-%m-%d %H:%M:%S UTC')}"
                )
            else:
                comp.df = pd.concat(comp.dfList, axis=1)  # Concat all the columns in the frame, aligning by time index
                comp.df = comp.df.groupby(level=0, axis=1).first().ffill()  # Merge same named columns by grouping using none n.a values
                comp.dfList.clear()

        return sdf

    def updateSensorDataByFile(
        self,
        asset: EdisonGenericComponent,
        sensorList: list,
        fileType: str = "edi",
        startDate: int = 0,
        endDate: int = 0,
        updateMethod: dataUpdateMethod = dataUpdateMethod.REPLACE,
        dirtyDelete: bool = False,
        quiet: bool = False,
        sync: bool = False
    ):
        """
        Updates the data of an ``asset`` by packaging the specified sensors of
        its [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) into a file and sending it to Edison.

        While updating, if ``overwriteData`` is set to False, it may cause unexpected behaviour. While using this method, be careful
        to ensure that the data is new, or ``overwriteData`` is set to True.

        Args:
            asset (EdisonGenericComponent): ``asset`` with an updated
                [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html).
            sensorList (list): List of names of sensors that are to be updated.
            fileType (str, optional): To-do. Defaults to "edi".
            startDate (int, optional): POSIX timestamp in milliseconds, start
                bracket for sensor data range. If left by default, the earliest
                timestamp of the Dataframe will be used.
            endDate (int, optional): POSIX timestamp in milliseconds, end
                bracket for sensor data range. If left by default, the latest
                timestamp of the Dataframe will be used.
            updateMethod (dataUpdateMethod, optional): Parameter to set the update method. Select from REPLACE, INSERT, or APPEND. Defaults to REPLACE.
            dirtyDelete (bool, optional): Flag to allow for quick deletion of
                data within the assets when uploading new values. Defaults to
                False.
            quiet (bool, optional): Flag to run the method quietly. Defaults to False.
            sync (bool, optional): Flag to run the method synchronously by waiting for upload tasks to complete.

        Returns:
            pandas.DataFrame: [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) containing time
            indexed values for sensors specified.
            `None` is returned if the serial-number does not correspond to
            any ``asset`` registered with the platform.

        Warning:
            If ``updateMethod == dataUpdateMethod.REPLACE``, any pre-existing data within the ``startDate`` and ``endDate`` for the
            specified sensors in the list will be **ERASED**.

        Raises:
            AssertionError if expected data lenght is not equal to the data length in the dataframe.
        """
        print("\nSENDING DATA TO SERVER:")
        if startDate == 0:
            startDate = asset.df.index[0]
        if endDate == 0:
            endDate = asset.df.index[-1]

        subdf = asset.df.loc[startDate:endDate, :].copy()
        subdf["time"] = subdf.index
        subdf["ts"] = (subdf["time"] - datetime(1970, 1, 1)).dt.total_seconds() * 1000
        payloadToSend = {"serialNumber": asset.serial_number, "data": []}
        sensorsId = {"sensorId": "xyz", "sensorPosition": "0"}
        sensorCodes = []
        for s in sensorList:
            sensor = getSensorByName(asset.model, s)
            if sensor is None:
                raise KeyError(f"Sensor {s} is not a valid sensor associated with the model {asset.model.name} of asset {asset.serial_number}")
            else:
                # After we have the sensor, we need to check if the array map is 1 or more.
                # The array map is a string. So first, convert it to an int.
                # Now, creating a for loop, we can quickly accomodate for any array size.
                # If sensor array size is 1, the loop runs once and appends the sensorId to the sensorCodes list.
                # If sensor array size is greater than 1, the loop runs multiple times and appends a new sensorId to the sensorCodes list.
                for i in range(int(sensor.array_map)):
                    sensorsId["sensorId"] = sensor.sensor_model.code
                    sensorsId["sensorPosition"] = str(i)
                    sensorCodes.append(sensorsId.copy())

        if not quiet:
            print("Preparing payload")

        if fileType == "edi":
            ss = []
            ss.append("ts")
            for s in sensorList:
                ss.append(s)

            codes = []
            codes.append("ts")
            # Now, if sensor array size is greater than 1, it will still get the correct sensor position.
            for s in sensorCodes:
                codes.append(s["sensorId"] + "|" + s["sensorPosition"])

            # Since sensorList is used to form codes (sensorCodes are formed using sensorList), and ss, the
            # sensors will always be aligned.
            for row in subdf[ss].values:
                record = {}
                row = row.tolist()  # row has to be an iterable. Any iterable will do. Using list as it has pop.
                # Fixing up the list of values such that all are in a sequence instead of values among lists.
                for i, x in enumerate(row):
                    if isinstance(x, Iterable):  # If the item is an iterable
                        temp = reversed(row.pop(i))  # pop it and flip it
                        for j in temp:  # list it out
                            row.insert(i, j)  # insert it back in the row.
                if len(row) != len(codes):  # confirm sizes
                    raise AssertionError(f"Number of values in DataFrame do not match the number of sensors (including sensor arrays). Expected {len(codes)}. Received {len(row)}.")
                for index, col in enumerate(row):
                    if not isnan(col):
                        # For each code in codes, create a key in record that is given the value of the row.
                        # Since row has been fixed, it is now possible to blindly append as everything is kosher.
                        record[codes[index]] = col

                if len(record.keys()) > 1:
                    payloadToSend["data"].append(record)

            f = open("data.edi", "a")
            f.truncate(0)
            for i in payloadToSend["data"]:
                f.write(json.dumps(i) + "\n")
            f.close()
            files = {"data": open("data.edi", "rb")}
            f = open("data.edi", "rb")

        elif fileType == "csv":
            renamingDict = {}
            for s in sensorCodes:
                renamingDict[asset.serial_number + getSensorNameByCode(asset.model, s["sensorId"])] = s["sensorId"]

            subdf.rename(columns=renamingDict, inplace=True)
            subdf.drop(columns="time", inplace=True)

            subdf.to_csv("data.csv", index=None, header=True, float_format="%.3f")

        if updateMethod == dataUpdateMethod.REPLACE:
            # Delete only if replace.
            sensorsToBeErased = {
                "serialNumbers": [asset.serial_number],
                "sensorCodes": sensorCodes,
                "startDate": startDate.value / 1000000,
                "endDate": endDate.value / 1000000,
                "isCleanDelete": not dirtyDelete,
            }

            # Delete sensor data to avoid overlaps
            resp = self.deleteSensorData(sensorsToBeErased)
            self.waitForTaskToComplete(resp)

        if updateMethod == dataUpdateMethod.APPEND:
            # Add the append field only if append
            fields = {"serialNumber": asset.serial_number, "append": 1}
        else:
            fields = {"serialNumber": asset.serial_number}

        files = {"data": ("data." + fileType, open("data." + fileType, "r").read())}

        # Dropping assignment as never used
        print("Sending payload to server...")
        resp = self.iotApi.uploadFile(files, fields)
        print("... file uploaded")

        if sync:
            self.waitForTaskToComplete(resp["task"])
        if not quiet:
            print(f"Payload sent successfully! Checkout: {os.environ.get('ALTERGO_FACTORY_API', self.domain)}core/iot/connected_component/view/{asset.id}/graph")

    def uploadDatasetToAsset(
        self,
        asset: EdisonGenericComponent,
        dataFrame: pd.DataFrame,
        dataSetDescription: str = "None",
        datasetName : str = "None",
        fileType: str = "csv",
        fileName: str = "default_dataset"
    ):
        """
            Attaches a Dataset to an Asset

            Args:
                asset (EdisonGenericComponent): The ``asset`` that needs to be updated.
                dataFrame (DataFrame): Pandas dataframe that will be converted to CSV
                dataSetDescription (str, Optional): Description of your dataset
                datasetName (str, optional): Name of your dataset
                    milliseconds. Defaults to 0.
                fileType (int, str): File format of your file ('.csv', '.json', etc...)
                    Defaults to 0.
                fileName (bool, optional): Name of the file


            """

        fields = {
            "name": datasetName,
            "description": dataSetDescription,
            "entities[0][entityType]": "asset",
            "entities[0][entityId]": asset.id
        }

        file = None
        if fileType == "csv":
            file

        fileName = fileName + "." + fileType

        file = open(fileName, "a")
        file.truncate(0)
        file.write(dataFrame.to_csv(index=False))
        file.close()

        files = {"datasetFile": (fileName, open(fileName, "r").read())}
        resp = self.iotApi.uploadDatasetToAsset(files, fields)          
        print(resp)

    def uploadDatasetToActivity(
        self,
        activity: Activity,
        dataFrame: pd.DataFrame,
        dataSetDescription: str = "None",
        datasetName : str = "None",
        fileType: str = "csv",
        fileName: str = "default_dataset"
    ):
        """
            Attaches a Dataset to an Activity

            Args:
                asset (EdisonGenericComponent): The ``asset`` that needs to be updated.
                activity (Activity): Pandas dataframe that will be converted to CSV
                dataSetDescription (str, Optional): Description of your dataset
                datasetName (str, optional): Name of your dataset
                    milliseconds. Defaults to 0.
                fileType (int, str): File format of your file ('.csv', '.json', etc...)
                    Defaults to 0.
                fileName (bool, optional): Name of the file


            """
        fields = {
            "name": datasetName,
            "description": dataSetDescription,
            "entities[0][entityType]": "activity",
            "entities[0][entityId]": activity.id
        }

        file = None
        if fileType == "csv":
            file

        fileName = fileName + "." + fileType

        file = open(fileName, "a")
        file.truncate(0)
        file.write(dataFrame.to_csv(index=False))
        file.close()

        files = {"datasetFile": (fileName, open(fileName, "r").read())}
        resp = self.iotApi.uploadDatasetToAsset(files, fields)          
        print(resp)

    def updateSensorDataByDirectInsert(
        self,
        asset: EdisonGenericComponent,
        sensorList: list,
        taskId: int = 0,
        startDate: int = 0,
        endDate: int = 0,
        quiet: bool = False,
    ):
        """
        Uploads the data of an ``asset`` by directly inserting the sensor data
        within the start date and end date time period.

        Args:
            asset (EdisonGenericComponent): The ``asset`` that needs to be updated.
            sensorList (list): List of sensors that are to be updated.
            taskId (int, optional): To-do. Defaults to 0.
            startDate (int, optional): Start date as POSIX time in
                milliseconds. Defaults to 0.
            endDate (int, optional): End date as POSIX time in milliseconds.
                Defaults to 0.
            quiet (bool, optional): Flag to run the method quietly. Defaults to False.

        Note:
            Direct insert does not support an array of sensors.

        """

        if startDate == 0:
            startDate = asset.df.index[0]
        if endDate == 0:
            endDate = asset.df.index[-1]

        subdf = asset.df.loc[startDate:endDate, :].copy()
        subdf["time"] = subdf.index
        subdf["ts"] = (subdf.loc[:, "time"] - datetime(1970, 1, 1)).dt.total_seconds() * 1000
        payloadToSend = {"serialNumber": asset.serial_number, "data": []}
        sensorsId = {"sensorId": "xyz", "sensorPosition": "0"}
        sensorCodes = []
        for s in sensorList:
            sensor = getSensorByName(asset.model, s)
            if sensor is None:
                raise KeyError(f"Sensor {s} is not a valid sensor associated with the model {asset.model.name} of asset {asset.serial_number}")
            else:
                for i in range(int(sensor.array_map)):
                    sensorsId["sensorId"] = sensor.sensor_model.code
                    sensorsId["sensorPosition"] = str(i)
                    sensorCodes.append(sensorsId.copy())

        if not quiet:
            print("\nPreparing payload")

        ss = []
        ss.append("ts")
        for s in sensorList:
            ss.append(s)

        codes = []
        codes.append("ts")
        for s in sensorCodes:
            codes.append(s["sensorId"] + "|" + s["sensorPosition"])

        # dataToSend = subdf[ss].values
        for row in subdf[ss].values:
            record = {}
            row = row.tolist()

            for i, x in enumerate(row):
                if isinstance(x, Iterable):  # If the item is an iterable
                    temp = reversed(row.pop(i))  # pop it and flip it
                    for j in temp:  # list it out
                        row.insert(i, j)  # insert it back in the row.
            if len(row) != len(codes):  # confirm sizes
                raise AssertionError(f"Number of values in DataFrame do not match the number of sensors (including sensor arrays). Expected {len(codes)}. Received {len(row)}.")

            for index, col in enumerate(row):
                if not isnan(col):  # Handles NaNs
                    record[codes[index]] = col
            if len(record.keys()) > 1:
                payloadToSend["data"].append(record)
                payloadToSend["append"] = 0
                body = json.dumps(payloadToSend)
                if len(body) > 100000:
                    body = json.dumps(payloadToSend)
                    # Dropping assignment as never used.
                    self.iotApi.sendSensorData(body)
                    if taskId != 0:
                        print(
                            "Payload sent :",
                            ((row + 1) / (len(subdf[ss].values) + 1)) * 100,
                            "%",
                        )
                        # Dropping assignment as never used
                        self.iotApi.updateTaskInfo(
                            json.dumps(
                                {
                                    "taskId": taskId,
                                    "status": "PROCESSING",
                                    "completion": ((row + 1) / (len(subdf[ss].values) + 1)) * 100,
                                }
                            )
                        )
                    payloadToSend["data"] = []

        body = json.dumps(payloadToSend)
        # Dropping assignment as never used.
        self.iotApi.sendSensorData(body)
        if not quiet:
            print("Payload sent successfully!\n")

    # def terminateSohTask(self, taskId):

    #     # ! Does this belong in the Ion SDK?

    #     payloadToSend = {"taskId": taskId}
    #     body = json.dumps(payloadToSend)
    #     # Removing assignment as not used
    #     self.iotApi.terminateSohTask(body)

    def createAsset(self, newAsset: EdisonGenericComponent) -> dict:
        """
        Create an ``asset`` on the Edison platform using an existing ``EdisonGenericComponent``.

        Args:
            newAsset (EdisonGenericComponent): EdisonGenericComponent used to
                create a new ``asset``.

        Returns:
            dict: Response from the Edison API.
        """

        payload = {
            "serialNumber": newAsset.serial_number,
            "componentStateCircumstantialStatus": "In Use",
            "componentStateLocation": "Unknown",
            "componentStateDescription": newAsset.current_state.description,
            "componentStateFunctionalStatus": "Available",
            "componentStateExistentialStatus": "Complete",
            "componentModelName": newAsset.model.name,
            "componentStateData": "",
        }

        resp = self.factoryApi.addComponent(payload)
        return json.loads(resp.text)

    def changeAssetState(self, newAsset: EdisonGenericComponent) -> dict:
        """
        Modify the ``assets``'s state through an updated ``asset``.

        Args:
            newAsset (EdisonGenericComponent): EdisonGenericComponent that needs to
                modified.

        Returns:
            dict: Response from the Edison API.
        """

        payload = {
            "circumstantialStatus": newAsset.current_state.status.name,
            "functionalStatus": newAsset.current_state.status_functional.name,
            "existentialStatus": newAsset.current_state.status_existential.name,
            "location": newAsset.current_state.location,
            "description": newAsset.current_state.description,
            "data": "",
        }

        resp = self.factoryApi.changeComponentState(newAsset.serial_number, payload)
        if resp.status_code != 200:
            raise SystemError(f"Failed to create an asset. Server responded with a status code of {resp.status_code}.")
        return json.loads(resp.text)

    def deleteAsset(self, existingAsset: EdisonGenericComponent) -> dict:
        """
        Delete an ``asset`` from the Edison platform.

        Args:
            existingAsset (EdisonGenericComponent): EdisonGenericComponent that
                needs to be deleted.

        Returns:
            dict: Response from the Edison API.

        Warning:
            Once an ``asset`` is deleted, it **cannot** be recovered.
        """

        resp = self.factoryApi.removeComponent(existingAsset.serial_number)
        return json.loads(resp.text)

    # response = requests.request(
    #       "POST",
    #       urlAddComponent,
    #       data=payloadAddComponent,
    #       headers=headersAddComponent
    # )

    def updateAssetSentinelParameters(self, assetSentinelParameters: Body):

        resp = self.iotApi.updateComponentSentinelParameters(assetSentinelParameters)
        return json.loads(resp.text)

    def createAssetBySerial(self, modelName: str, serialNumber: str) -> dict:
        """
        Create an ``asset`` using a Serial Number.

        Args:
            modelName (str): Name of the model from which the ``asset`` is
                made.
            serialNumber (str): Unique serial number to identify the ``asset``.

        Returns:
            dict: Response from the Edison API.
        """

        model = Model()
        currentState = CurrentState()
        model.name = modelName
        newComp = EdisonGenericComponent()
        newComp.model = model
        newComp.serial_number = serialNumber
        newComp.current_state = currentState
        newComp.current_state.description = "First upload"

        resp = self.createAsset(newComp)
        return resp

    def deleteAssetBySerial(self, serialNumber: str) -> dict:
        """
        Delete an ``asset`` by Serial Number.

        Args:
            serialNumber (str): Serial Number of the asset that must be
                deleted.

        Returns:
            dict: Response from the Edison API.

        Warning:
            Once an ``asset`` is deleted, it **cannot** be recovered.
        """
        model = Model()
        currentState = CurrentState()
        newComp = EdisonGenericComponent()
        newComp.model = model
        newComp.serial_number = serialNumber
        newComp.current_state = currentState
        newComp.current_state.description = "First upload"

        resp = self.deleteAsset(newComp)
        return resp

    def matchSensors(self, sensorArray: list, asset: EdisonGenericComponent) -> list:
        """
        Method to match sensors within the ``asset`` and the list of sensors.

        Args:
            sensorArray (list): List of sensors which needs to be matched with
                the column names of the [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) of the
                ``asset``.
            asset (EdisonGenericComponent): The EdisonGenericComponent for which the sensors
                have to be mathched.

        Returns:
            list: List of names of sensors after refactoring.
        """
        sensorNames = []
        refactoredSensors = []
        for s in asset.model.sensors:
            sensorNames.append(s.sensor_model.name)
        for sensor in sensorArray:
            str2Match = sensor
            highest = process.extractOne(str2Match, sensorNames)
            print(highest)
            if highest[1] > 90:
                sensor = highest[0]
            else:
                sensor = highest[0]
                print("WARNING Possible Sensor Name Mismatch !!!")
            refactoredSensors.append(sensor)
        return refactoredSensors

    def refactorDataframeToAsset(self, df: pd.DataFrame, asset: EdisonGenericComponent, sensors: dict = {}) -> list:
        """
        Refactor the columns of the [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) with the
        correct names of the sensors associated with the asset.

        Args:
            df (pd.DataFrame): [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) that has to be
                refactored.
            asset (EdisonGenericComponent): EdisonGenericComponent for which the
                [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) must be refactored.
            sensors (dict, optional): To-do. Defaults to {}.

        Returns:
            list: Refactored list of sensor names.
        """
        # TODO: Add Example to docstring
        sensorNames = []
        sensorUploadList = []
        for s in asset.model.sensors:
            sensorNames.append(s.sensor_model.name)
        for key in df.columns:
            str2Match = key

            highest = process.extractOne(str2Match, sensorNames)
            print(highest)
            if highest[1] > 90:
                df.rename(columns={key: highest[0]}, inplace=True)
            else:
                # df.rename(columns={key: highest[0]}, inplace=True)
                print("WARNING Possible Sensor Name Mismatch !!!")
            if sensors != {}:
                for key in sensors:
                    if sensors[key].synced == 1:
                        sensorUploadList.append(highest[0])
            else:
                sensorUploadList.append(highest[0])

        return sensorUploadList

    def deleteSensorData(self, query):
        """
        Delete a sensor data by serial numbers and sensor codes.

        Args:
            query (str): Serial Numbers and sensor codes to be erased.

        Returns:
            Deletion results
        """
        sensorArray = []
        sensorArray.append(query)
        body = json.dumps(sensorArray)
        resp = self.iotApi.deleteSensorData(body)
        task = resp.get("task", None)
        if task is None:
            raise RuntimeError("A previous task is currently active. Please wait till the task ends. To see a list of active tasks, go to https://edison.ionenergy.co/core/iot/tasks/view")
        return task

    def eraseAllData(self, asset: EdisonGenericComponent) -> None:
        # """
        # Erase all data associated with an asset.

        # Args:
        #     asset (EdisonGenericComponent): EdisomComponent from which all data must be deleted.

        # Returns:
        #     None
        #
        # Note:
        #     Data
        # """
        query = json.dumps(
            {
                "serialNumber": asset.serial_number,
            }
        )

        resp = self.iotApi.eraseAllData(query)

        task_info = resp.json().get("task", None)
        task_info = self.waitForTaskToComplete(task_info)
        if task_info is None or task_info.get("status", None) != "FINISHED":
            UserWarning(f"Could not erase all data on serial number {asset.serial_number}. Please try again.")
        else:
            print(f"All data in asset with serial number {asset.serial_number} has been deleted")

        return None

    def eraseAllDataBySerial(self, serialNumber: str) -> None:
        # """
        # Erase all data associated with an asset with the given serial number.
        #
        # Args:
        #     serialNumber (str): Serial Number of asset for which all data must be erased.
        #
        # Returns:
        #     None
        # """

        model = Model()
        currentState = CurrentState()
        newComp = EdisonGenericComponent()
        newComp.model = model
        newComp.serial_number = serialNumber
        newComp.current_state = currentState
        newComp.current_state.description = "First upload"

        self.eraseAllData(newComp)

        return None

    def waitForTaskToComplete(self, task):
        # """
        # Wait for a task to complete.

        # Args:
        #     query (str): task object.

        # Returns:
        #     Task result
        # """
        # ! Has this been deprecated?

        print("Waiting for", task["type"], "task to complete...")
        while True:
            taskProcess = self.factoryApi.getTaskById(task["id"])
            if taskProcess["isTerminated"]:
                print(task["type"], "task completed!")
                break
            else:
                time.sleep(1)        
        return taskProcess


def setupClient():
    """
    Setup an EdisonClient using a password text prompt for the API key. The function allows users to setup the EdisonClient without leaving the API key in the code.

    Returns:
        Client: A Client with the API Key already set up.

    Example:
    ```python
    from ion_sdk.edison_api.edison_api import setupClient
    edApi = setupClient()
    Enter your api Key: (go to " + "https://edison.ionenergy.co/core/users/me/api-key/) **********
    ```
    """
    base_url = os.environ.get("ALTERGO_FACTORY_API", None)
    if base_url is None:
        UserWarning(
            """Could not find Factory API Endpoints in Environment Variables. Using Edison Defaults instead.\n
            To provide API Endpoints in the Environment Variables, refer to https://ion-energy.gitlab.io/edison-kb-private/docs/edison/library/ion-sdk/installation."""
        )
        base_url = "https://edison.ionenergy.co/"
    apiKey = getpass.getpass(
        prompt="Enter your api Key: (go to " + f"{base_url}core/users/me/api-key) ",
        stream=None,
    )
    return Client(apiKey)


def edisonDate(yy, MM=1, dd=1, hh=0, mm=0, s=0, ms=0):
    """
    Converts a date to an Edison compatible date. Note that the time is unaware
    of the timezone and will assume the timezone of the local machine.

    Args:
        yy (int): Year
        MM (int, optional): Month. Defaults to 1.
        dd (int, optional): Day. Defaults to 1.
        hh (int, optional): Hour. Defaults to 0.
        mm (int, optional): Minute. Defaults to 0.
        s (int, optional): Second. Defaults to 0.
        ms (int, optional): Millisecond. Defaults to 0.

    Returns:
        float: POSIX time in milliseconds.

    Examples:
    ```python
    from ion_sdk.edison_api.edison_api import edisonDate
    edisonDate(2020,1,1,0,0,0) # For GMT+5:30
    1577817000000.0
    ```

    """
    return datetime.timestamp(datetime(yy, MM, dd, hh, mm, s, ms)) * 1000
