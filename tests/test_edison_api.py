import sys
import datetime
import numpy as np
import pandas as pd
import time

sys.path.append(".")
import ion_sdk.edison_api.edison_api as eapi
from ion_sdk.edison_api.models.factoryModel import (
    EdisonGenericComponent,
    Model,
    CurrentState,
)
from ion_sdk.tools.toolbox import decimate_data

apiKey = "d365fd18cd5a14c1965349b6aa2f9253"  # Dummy Account API
endpoit = "https://altergo.io"
iotEndpoint = "https://iot.altergo.io"


def test_ComponentApi():
    print(eapi.__file__)
    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    testSerialNumber = "Bridges"
    resp = edApi.getAsset(testSerialNumber)
    if resp is not None:
        resp = edApi.deleteAsset(resp)

    model = Model()
    currentState = CurrentState()
    model.name = "Foo"
    newComp = EdisonGenericComponent()
    newComp.model = model
    newComp.serial_number = testSerialNumber
    newComp.current_state = currentState
    newComp.current_state.description = "test description"

    resp = edApi.createAsset(newComp)
    assert resp["status"] == 1
    comp = edApi.getAsset(testSerialNumber)
    assert comp.serial_number == "Bridges"
    resp = edApi.deleteAsset(newComp)
    assert resp["message"] == "Component has been removed."
    # Test limited getMultipleAssets
    resp = edApi.getAssets("FBP", 100)
    assert len(resp) > 0  
    comp = edApi.getAsset("Parent")
    assert len(comp.model.child_component_models) > 0
    assert len(comp.current_state.child_components) > 0


def test_edisonDate():

    # TZ Offset
    offset = (datetime.datetime.now() - datetime.datetime.utcnow()).total_seconds() * 1000
    if offset < 0:
        offset = 0
    if np.floor(offset) % 10 == 0:
        offset = np.floor(offset)  # Zeroing any spurious nanoseconds
    else:
        offset = np.ceil(offset)    
    print(eapi.edisonDate(2020, 1, 1, 0, 0, 0))
    op = 1577836800000.0 - offset
    print(op)

    assert eapi.edisonDate(2020, 1, 1, 0, 0, 0) == 1577836800000.0 - offset


def test_example_use_case():

    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    testSerialNumber = "Bash"
    # If asset exists, delete it.
    resp = edApi.getAsset(testSerialNumber)
    if resp is not None:
        resp = edApi.deleteAsset(resp)

    # Create an asset by serial number
    resp = edApi.createAssetBySerial("Foo", testSerialNumber)
    assert resp["status"] == 1

    # Get an asset
    comp = edApi.getAsset(testSerialNumber)
    assert comp.serial_number == "Bash"

    # Send new data on the voltage sensor with the append method and no overwrite.
    np.random.seed(5678)  # Setting a different seed to ensure that new data will be generated.
    example_df_append = pd.DataFrame(
        {"Voltage": [[3.6 + 0.01 * np.random.rand(), 3.6 + 0.01 * np.random.rand()] for _ in range(1, 21)]}, index=pd.date_range(start="2021-04-15", periods=20, freq="S", name="time")
    )
    comp.df = example_df_append
    temp = example_df_append.copy()
    uploadSensorList = ["Voltage"]
    edApi.updateSensorDataByFile(comp, uploadSensorList, updateMethod=eapi.dataUpdateMethod.INSERT, sync=True)

    # Pull the same data
    comp = edApi.getAsset(testSerialNumber)
    # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
    edApi.getAssetDataFrame([comp], ["Voltage"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    v0 = []
    v1 = []
    for val in example_df_append["Voltage"]:
        v0.append(val[0])
        v1.append(val[1])
    # for i, val in enumerate(temp["Voltage"]):
    #     v0[i] = val[0]
    #     v1[i] = val[1]
    example_df_append["Voltage|0"] = v0
    example_df_append["Voltage|1"] = v1
    example_df_append.drop("Voltage", axis=1, inplace=True)
    print(example_df_append)
    print(comp.df)
    assert comp.df.equals(example_df_append)
    if comp.df.equals(example_df_append):
        print('equal')

    # Send data on the voltage sensor. This sensor has an array size of 2, thus working the array extension code.
    np.random.seed(1234)
    example_df = pd.DataFrame(
        {"Voltage": [[3.2 + 0.01 * np.random.rand(), 3.2 + 0.01 * np.random.rand()] for _ in range(1, 11)]}, index=pd.date_range(start="2021-04-15", periods=10, freq="S", name="time")
    )
    comp.df = example_df
    uploadSensorList = ["Voltage"]
    edApi.updateSensorDataByFile(comp, uploadSensorList, sync=True)

    # Pull the same data
    comp = edApi.getAsset(testSerialNumber)
    # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
    edApi.getAssetDataFrame([comp], ["Voltage"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    v0 = []
    v1 = []
    for val in example_df["Voltage"]:
        v0.append(val[0])
        v1.append(val[1])
    for val in temp["Voltage"].iloc[10:]:
        v0.append(val[0])
        v1.append(val[1])
    temp.loc[:, "Voltage|0"] = v0
    temp.loc[:, "Voltage|1"] = v1
    temp.drop("Voltage", axis=1, inplace=True)

    print(temp)
    print(comp.df)
    assert comp.df.equals(temp)

    # Send data for Current to test direct insert
    np.random.seed(1234)
    example_df = pd.DataFrame({"Current": [3.6 + 0.01 * np.random.rand() for _ in range(1, 11)]}, index=pd.date_range(start="2021-04-15", periods=10, freq="S", name="time"))
    comp.df = example_df
    uploadSensorList = ["Current"]  # Sensor ID for Current
    edApi.updateSensorDataByDirectInsert(comp, uploadSensorList)

    # Pull the same data
    comp = edApi.getAsset(testSerialNumber)
    # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
    edApi.getAssetDataFrame([comp], ["Current"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    v0 = []
    for val in example_df["Current"]:
        v0.append(val)
    example_df["Current"] = v0
    # example_df.drop("Dt7", axis=1, inplace=True)
    assert comp.df.equals(example_df)

    # Erase all data in the asset
    edApi.eraseAllDataBySerial(testSerialNumber)

    # Confirm all data has been erased
    try:
        edApi.getAssetDataFrame([comp], ["Current"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    except ValueError:
        pass

    # Delete asset
    resp = edApi.deleteAssetBySerial(testSerialNumber)
    assert resp["message"] == "Component has been removed."


# def test_gappy_data():

#     """
#     This test tests out the cases of uploading data to cases where gaps in between data need to be filled.
#     """

#     edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

#     testSerialNumber = "Blitz"
#     # If asset exists, delete it.
#     resp = edApi.getAsset(testSerialNumber)
#     if resp is not None:
#         resp = edApi.deleteAsset(resp)

#     # # Create an asset by serial number
#     resp = edApi.createAssetBySerial("Foo", testSerialNumber)
#     assert resp["status"] == 1

#     # Get an asset
#     comp = edApi.getAsset(testSerialNumber)
#     assert comp.serial_number == "Blitz"

#     # Send a set of data to the platform. This forms a gap in the middle
#     np.random.seed(1234)
#     example_df = pd.DataFrame(
#         {"Voltage": [[3.6 + 0.01 * np.random.rand(), 3.6 + 0.01 * np.random.rand()] for _ in range(1, 11)]}, index=pd.date_range(start="2021-06-08", periods=10, freq="T", name="time")
#     )
#     # temp = example_df.copy()
#     comp.df = example_df.iloc[[0, 1, 8, 9]]
#     uploadSensorList = ["Voltage"]
#     edApi.updateSensorDataByFile(comp, uploadSensorList, sync=True)  # SENT IN REPLACE MODE

#     # Now try to append data to the same asset.
#     np.random.seed(5678)
#     example_df_append = pd.DataFrame(
#         {"Voltage": [[3.6 + 0.01 * np.random.rand(), 3.6 + 0.01 * np.random.rand()] for _ in range(1, 21)]}, index=pd.date_range(start="2021-06-08", periods=20, freq="T", name="date")
#     )
#     # temp = example_df.copy()
#     comp.df = example_df_append
#     uploadSensorList = ["Voltage"]
#     edApi.updateSensorDataByFile(comp, uploadSensorList, updateMethod=eapi.dataUpdateMethod.APPEND, sync=True)  # SENT IN APPEND MODE

#     # Send the data in the gap
#     comp.df = example_df.iloc[1:9]
#     uploadSensorList = ["Voltage"]
#     edApi.updateSensorDataByFile(comp, uploadSensorList, updateMethod=eapi.dataUpdateMethod.INSERT, sync=True)  # SENT IN INSERT MODE

#     # Pull the same data
#     comp = edApi.getAsset(testSerialNumber)
#     # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
#     edApi.getAssetDataFrame([comp], ["Voltage"], eapi.edisonDate(2021, 6, 5, 0, 0, 0), eapi.edisonDate(2021, 6, 10, 0, 0, 0))
#     v0 = []
#     v1 = []
#     for val in example_df["Voltage"]:
#         v0.append(val[0])
#         v1.append(val[1])
#     for val in example_df_append["Voltage"][10:]:
#         v0.append(val[0])
#         v1.append(val[1])

#     example_df_append["Voltage|0"] = v0
#     example_df_append["Voltage|1"] = v1
#     example_df_append.drop("Voltage", axis=1, inplace=True)
#     print(example_df_append)
#     print(comp.df)
#     # assert comp.df.equals(example_df_append)

#     # Erase all data in the asset
#     edApi.eraseAllDataBySerial(testSerialNumber)

#     # Confirm all data has been erased
#     try:
#         edApi.getAssetDataFrame([comp], ["Voltage"], eapi.edisonDate(2021, 6, 7, 0, 0, 0), eapi.edisonDate(2021, 6, 9, 0, 0, 0))
#         assert False, 'Data were found. Hence, EraseAllData function failed to delete the dataset of the test'
#     except ValueError:
#         print('Dataset have been removed correctly.')
#         assert True

#     # Delete asset
#     resp = edApi.deleteAssetBySerial(testSerialNumber)
#     assert resp["message"] == "Component has been removed."


def test_decimation():
    """
    The test is to test the decimation algorithm.
    """

    np.random.seed(1234)
    ser = pd.Series(
        data=[x + np.random.rand() for x in range(0, 100, 1)],
        index=[x for x in pd.date_range(datetime.datetime(2021, 7, 10), periods=100, freq="1S")],
    )
    df = decimate_data(ser, val_threshold=4e0, time_threshold=datetime.timedelta(seconds=5))
    df2 = df.reset_index().diff()
    assert (df2.loc[df2["index"] < datetime.timedelta(seconds=5), "Decimated Series"] >= 4e0).all()
    assert (df2.loc[df2["Decimated Series"] < 4e0, "index"] >= datetime.timedelta(seconds=5)).all()


def test_nan_data():
    """
    This test is to verify that NaNs in the data sequence are safely handled by the SDK.
    """

    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    testSerialNumber = "Baseball"
    # If asset exists, delete it.
    resp = edApi.getAsset(testSerialNumber)
    if resp is not None:
        resp = edApi.deleteAsset(resp)

    # Create an asset by serial number
    resp = edApi.createAssetBySerial("Foo", testSerialNumber)
    assert resp["status"] == 1

    # Get an asset
    comp = edApi.getAsset(testSerialNumber)
    assert comp.serial_number == "Baseball"

    # Send new data on the voltage sensor with the append method and no overwrite.
    np.random.seed(5678)  # Setting a different seed to ensure that new data will be generated.
    example_df_append = pd.DataFrame(
        {"Voltage": [[3.6 + 0.01 * np.random.rand(), 3.6 + 0.01 * np.random.rand()] for _ in range(1, 11)]}, index=pd.date_range(start="2021-04-15", periods=10, freq="S", name="time")
    )
    for _ in range(5):
        row_idx = np.random.randint(0, 10)
        val_to_modify = example_df_append.iloc[row_idx, 0]
        val_to_modify[np.random.randint(0, 2)] = np.NaN
        example_df_append.iloc[row_idx, 0] = val_to_modify
    comp.df = example_df_append
    uploadSensorList = ["Voltage"]
    edApi.updateSensorDataByFile(comp, uploadSensorList, updateMethod=eapi.dataUpdateMethod.INSERT, sync=True)

    # Pull the same data
    comp = edApi.getAsset(testSerialNumber)
    # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
    edApi.getAssetDataFrame([comp], ["Voltage"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    v0 = []
    v1 = []
    for val in example_df_append["Voltage"]:
        v0.append(val[0])
        v1.append(val[1])
    example_df_append["Voltage|0"] = v0
    example_df_append["Voltage|1"] = v1
    example_df_append.drop("Voltage", axis=1, inplace=True)
    example_df_append.dropna(how="all", inplace=True)
    example_df_append.ffill(inplace=True)
    assert comp.df.equals(example_df_append)

    # Send data for Current to test direct insert
    np.random.seed(1234)
    example_df = pd.DataFrame({"Current": [3.6 + 0.01 * np.random.rand() for _ in range(1, 11)]}, index=pd.date_range(start="2021-04-15", periods=10, freq="S", name="time"))
    for _ in range(5):
        example_df.iloc[np.random.randint(0, 10), 0] = np.NaN
    comp.df = example_df
    uploadSensorList = ["Current"]  # Sensor ID for Current
    edApi.updateSensorDataByDirectInsert(comp, uploadSensorList)

    # Pull the same data
    comp = edApi.getAsset(testSerialNumber)
    # This can be a narrow time-range. But if it is narrow, timezones have to be accounted for.
    edApi.getAssetDataFrame([comp], ["Current"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    v0 = []
    for val in example_df["Current"]:
        v0.append(val)
    example_df["Current"] = v0
    example_df.dropna(inplace=True)
    assert comp.df.equals(example_df)

    # Erase all data in the asset
    edApi.eraseAllDataBySerial(testSerialNumber)

    # Confirm all data has been erased
    try:
        edApi.getAssetDataFrame([comp], ["Current"], eapi.edisonDate(2021, 4, 14, 0, 0, 0), eapi.edisonDate(2021, 4, 16, 0, 0, 0))
    except ValueError:
        pass

    # Delete asset
    resp = edApi.deleteAssetBySerial(testSerialNumber)
    assert resp["message"] == "Component has been removed."


# def test_DataApi():
#
#     apiKey = "2fc1603feac7be9f4e52496e3ee33af2"
#     edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

#     # Create a list of serial number corresponding to the components you want to work with.
#     componentSnList = ["RBMS0"]
#     components = []
#     for c in componentSnList:
#         components.append(edApi.getAsset(c))

#     # Create a list of sensor names. The name should match existing edison sensors
#     sensorNameList = [
#         "Soc",
#         "Current",
#         "Rack Voltage",
#         "Avg Cell Voltage",
#         "Avg Module T",
#         "Min Module T",
#         "Max Module T",
#     ]
#     req = {
#         "components": components,
#         "sensorNames": sensorNameList,
#         "startDate": eapi.edisonDate(2019, 4, 10, 7, 00),
#         "endDate": eapi.edisonDate(2019, 4, 15, 9, 00),
#     }
#     edApi.getAssetDataFrame(**req)

#     for comp in components:
#         tb.proceduralPrediction(
#             comp, 6 * 3600, 24 * 3600 * 5, "Current", "Soc", "Avg Module T"
#         )

#         edge = tb.getEqCycles(comp)

#         eCparams = {
#             "eqCycleName": "eqCycle-current(EAI)-Predict",
#             "current": "Current-Predict",
#             "initEqCycle": edge["lastEqCvalue"],
#         }
#         edge = tb.getEqCycles(comp, **eCparams)

#         # initialize SOH for each component (could reuse SOH calculated from existing records)

#         edge = tb.getSOHfromPmodel(comp, temperature="Avg Module T")
#         params = {
#             "component": comp,
#             "temperature": "Avg Module T-Predict",
#             "soc": "Soc-Predict",
#             "eqCycles": "eqCycle-current(EAI)-Predict",
#             "sohName": "Soh-pModel(EAI)-Predict",
#             "initSoh": edge["lastSohValue"],
#             "initAgeSec": edge["lastSohDuration"],
#         }
#         tb.getSOHfromPmodel(**params)

#         tb.getRdc(comp, voltage="Rack Voltage")

#         uploadSensorList = [
#             "Soh-pModel(EAI)",
#             "Current-Predict",
#             "Soh-pModel(EAI)-Predict",
#             "Impedance-pModel(EAI)",
#             "eqCycle-current(EAI)",
#             "eqCycle-current(EAI)-Predict",
#         ]
#         edApi.updateSensorDataByFile(comp, uploadSensorList)

#     assert 1
