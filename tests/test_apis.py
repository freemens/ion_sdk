import sys
import datetime
import numpy as np
import pandas as pd
import time

sys.path.append(".")
import ion_sdk.edison_api.edison_api as eapi
from ion_sdk.edison_api.models.factoryModel import (
    ActivityMetadata,
    Tag,
    Activity,
    Dashboard,
    Model,
    CurrentState,
    EdisonGenericComponent
)
from ion_sdk.tools.toolbox import decimate_data

apiKey = "c4a6902fc5539a24e924b989755d1704"  # Dummy Account API
endpoit = "https://staging.altergo.io"
iotEndpoint = "https://iot.staging.altergo.io"


def test_ActivityApi():
    print(eapi.__file__)
    print("START TEST: Activity")
    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    # Create test asset
    testSerialNumber = "Bridges"
    resp = edApi.getAsset(testSerialNumber)
    if resp is not None:
        resp = edApi.deleteAsset(resp)

    model = Model()
    currentState = CurrentState()
    model.name = "Generic Cell"
    newComp = EdisonGenericComponent()
    newComp.model = model
    newComp.serial_number = testSerialNumber
    newComp.current_state = currentState
    newComp.current_state.description = "test description"
    resp = edApi.createAsset(newComp)

    # Create test tag
    tagName = "UNIT_TESTING_ACTIVITY_TAG_NAME"
    tags = edApi.getTagsByName(tagName)
    if len(tags) > 0:
        resp = edApi.removeTag(tags[0])

    newTag = Tag()
    newTag.name = tagName
    newTag.color = "danger"
    newTag.description = "UNIT_TESTING_ACTIVITY_TAG_DESCRIPTION"
    print("Test create tag API")
    createdTag = edApi.createTag(newTag)

    # Get All activities & remove them
    activities = edApi.getAllActivities()
    for act in activities:
        edApi.removeActivity(act)

    # Create new activity
    activity = Activity()
    activity.name = "UNIT_TESTING_ACTIVITY_NAME"
    activity.description = "UNIT_TESTING_ACTIVITY_DESCRIPTION"
    activity.location = "UNIT_TESTING_ACTIVITY_LOCATION"
    activity.startDate = time.time() * 1000
    activity.endDate = activity.startDate + 3600000
    activity.tags = []
    activity.tags.append(createdTag)
    activity.assets = []
    activity.assets.append(newComp)
    metadataField = ActivityMetadata()
    metadataField.metadataKey = "UNIT_TESTING_ACTIVITY_METADATA_KEY"
    metadataField.metadataValue = "UNIT_TESTING_ACTIVITY_METADATA_Value"
    activity.metadata = []
    activity.metadata.append(metadataField)

    print("Activities tests succeeded")


def test_TagApi():
    print(eapi.__file__)
    print("START TEST: Tags")

    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    tagName = "UNIT_TESTING_TAG_NAME"
    tags = edApi.getTagsByName(tagName)
    if len(tags) > 0:
        resp = edApi.removeTag(tags[0])

    newTag = Tag()
    newTag.name = tagName
    newTag.color = "danger"
    newTag.description = "UNIT_TESTING_TAG_DESCRIPTION"

    print("Test create tag API")
    createdTag = edApi.createTag(newTag)
    assert createdTag is not None
    tags = edApi.getTagsByName(tagName)
    assert len(tags) == 1

    tag = tags[0]
    assert tag.id == createdTag.id
    assert tag.name == createdTag.name
    assert tag.color == createdTag.color
    assert tag.description == createdTag.description

    # Test full changes
    print("Test edit API for all fields")
    editTagPayload1 = {
        "name": "UNIT_TESTING_TAG_NAME_1",
        "color": "warning",
        "description": "UNIT_TESTING_TAG_DESCRIPTION_1"

    }
    editedTag = edApi.editTag(tag, editTagPayload1)
    assert editedTag is not None
    assert editedTag.name == editTagPayload1["name"]
    assert editedTag.color == editTagPayload1["color"]
    assert editedTag.description == editTagPayload1["description"]

    # Test name change
    print("Test edit API for name")
    editTagPayload2 = {
        "name": "UNIT_TESTING_TAG_NAME_2",
    }
    editedTag = edApi.editTag(tag, editTagPayload2)
    assert editedTag is not None
    assert editedTag.name == editTagPayload2["name"]
    assert editedTag.color == editTagPayload1["color"]
    assert editedTag.description == editTagPayload1["description"]

    # Test description change
    print("Test edit API for description")
    editTagPayload3 = {
        "description": "UNIT_TESTING_TAG_DESCRIPTION_2"
    }
    editedTag = edApi.editTag(tag, editTagPayload3)
    assert editedTag is not None
    assert editedTag.name == editTagPayload2["name"]
    assert editedTag.color == editTagPayload1["color"]
    assert editedTag.description == editTagPayload3["description"]

    # Test color change
    print("Test edit API for color")
    editTagPayload4 = {
        "color": "brand",
    }
    editedTag = edApi.editTag(tag, editTagPayload4)
    assert editedTag is not None
    assert editedTag.name == editTagPayload2["name"]
    assert editedTag.color == editTagPayload4["color"]
    assert editedTag.description == editTagPayload3["description"]

    print("Test remove API")
    resp = edApi.removeTag(editedTag)
    assert resp["success"] is True

    tags = edApi.getTagsByName(editedTag.name)
    assert len(tags) == 0

    print("Tag tests succeeded")


def test_DashboardApi():   
    print(eapi.__file__)
    edApi = eapi.Client(apiKey, endpoit, iotEndpoint)

    dashboardName = "UNIT_TESTING_DASHBOARD_NAME_1"
    dashboards = edApi.getDashboardsByName(dashboardName)
    if len(dashboards) > 0:
        resp = edApi.removeDashboard(dashboards[0])

    tagName = "UNIT_TESTING_DASHBOARD_TAG_NAME_1"
    tags = edApi.getTagsByName(tagName)
    if len(tags) > 0:
        resp = edApi.removeTag(tags[0])

    tag = Tag()
    tag.name = tagName
    tag.color = "danger"
    tag.description = "UNIT_TESTING_DASHBOARD_TAG_DESCRIPTION_1"
    createdTag = edApi.createTag(tag)

    newDashboard = Dashboard()
    newDashboard.name = dashboardName
    newDashboard.tags = [createdTag]
    newDashboard.projectId = 1
    newDashboard.iconClass = "la la-bug"
    newDashboard.iconBgColor = "warning"
    newDashboard.targetedTemplateId = 4
    newDashboard.accessibility = "PUBLIC"
    newDashboard.isTemplate = True
    createdDashboard = edApi.createDashboard(newDashboard)
    assert createdDashboard is not None
    dashboards = edApi.getDashboardsByName(dashboardName)
    assert dashboards is not None

    editDashboard = dashboards[0]
    dashboardNewName = "UNIT_TESTING_DASHBOARD_NAME_2"
    editDashboard.name = dashboardNewName
    # editDashboard = edApi.editDashboard(editDashboard)
    # assert editDashboard is not None
    # assert editDashboard.name == dashboardNewName
    resp = edApi.removeDashboard(editDashboard)
    assert resp["success"] is True

    dashboards = edApi.getDashboardsByName(editDashboard.name)
    assert len(dashboards) == 0

    resp = edApi.removeTag(createdTag)
    assert resp["success"] is True
    tags = edApi.getTagsByName(createdTag.name)
    assert len(tags) == 0

    print("Dashboard tests succeeded")
