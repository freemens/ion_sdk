import ion_sdk
import ion_sdk.edison_api.edison_api as eapi
import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from datetime import datetime

# Parameters
activityId = -1
activityName = "HPPC Testing"

import warnings
warnings.filterwarnings('ignore')


def vDynModel( transient_time,V0, a, b, c, d):
    return V0-a*(1-np.exp(-b *  transient_time)) - c *(1-np.exp(-d *  transient_time))
    
def extractParameters(df):
    
    # HELPER FEATURES
    df['Current'] = df['Current'].fillna(method="backfill")
    df['time']=df.index
    df['dt']=df['time'].diff()
    df['dt']=df['dt'].dt.total_seconds()
    df['Im1']=df['Current'].shift(1)
    df['dI']=df['Current'].diff()
    df['dIm1']=df['dI'].shift(1)
    df['dIp1']=df['Current'].diff(-1)
    df['dV']=df['Voltage'].diff()
    df['dVm1']=df['dV'].shift(1)
    df['dVp1']=df['Voltage'].diff(-1)
    
    #SOC CALCULATION
    print('SoC Calculation...')
    df['Q']=df['Current']*df['dt']
    df['Capacity']=df['Q'].cumsum()/3600
    df['SOC']=(df['Capacity']-df['Capacity'].min())/(df['Capacity'].max()-df['Capacity'].min())
    
    # DECIMATION
    print('Decimation...')
    df=df[(abs(df['dV'])>0.0001) | (abs(df['dVp1'])>0.0001) |(abs(df['dI'])>0.05)| (abs(df['dIp1'])>0.05) ]
    
    # TIMESPAN SANITY CHECK
    print('Recalibrating dt...')
    df['dt']=df['time'].diff()
    df['dt']=df['dt'].dt.total_seconds()
    
    # TRANSIENT DETECTION
    print('Transient detection...')
    df[['relaxationStart','relaxationEnd','detdI','detV']] = df.apply(relaxationDetector, axis=1,result_type ='expand')
    df['relaxationStart']=df['relaxationStart'].shift(-1)
    df['relaxationEnd']=df['relaxationEnd'].shift(-1)
    df['relaxationStart']=df['relaxationStart'].ffill()
    df['relaxationEnd']=df['relaxationEnd'].ffill()
    df['detdI']=df['detdI'].ffill()
    df['detV']=df['detV'].ffill()
    
    # OCV GENERATION
    print('OCV Generation...')
    OCVdis=df[(df['relaxationEnd']==1) & (df['dIp1']>0)]
    OCVdis=OCVdis.sort_values(by=['SOC'])
    df['OCV'] = np.interp(df['SOC'],OCVdis['SOC'],OCVdis['Voltage'])
    df['Vdyn']=df['Voltage']-df['OCV']
    
    # START / STOP DISCHARGE DETECTION
    print('Discharge Start/Stop detection...')
    startofDischarge = df[df['Current']< 0].index[0]
    endofDischarge= df[df['Current']< 0].index[-1]
    
    # ISOLATE DISCHARGE PORTION
    print('Isolate discharge portion...')
    df_transient=df[startofDischarge:endofDischarge]
    # FOCUS on active transient
    print('Focus on active transient...')
    df_start= df_transient[df_transient.relaxationEnd==1]
    start_index=list(df_start.index.values)


    df_stop= df_transient[df_transient.relaxationStart==1]
    stop_index=list(df_stop.index.values)


    if len(start_index) > len(stop_index):
        start_index.pop(-1)
    elif len(stop_index) > len(start_index):
        stop_index.pop(0)

    i_start=df_transient.index.get_loc(start_index[0])
    i_stop=df_transient.index.get_loc(stop_index[0])
    
    #CALCULATE CURVE FITTING COEFFICIENTS
    print('Calculating Coeffs...')
    
    nrow=len(start_index)
    ncoeff=6
    coeff = np.zeros((nrow, ncoeff))

    for i in range(0,len(start_index)):

        start=start_index[i]
        stop=stop_index[i]

        i_start=df_transient.index.get_loc(start)
        i_stop=df_transient.index.get_loc(stop) 

        transient_time=df_transient['time'].iloc[i_start+1:i_stop]
        transient_time=transient_time.diff().dt.total_seconds().cumsum()
        transient_time=transient_time.replace(np.nan,0)
        transient_voltage=df_transient['Vdyn'].iloc[i_start+1:i_stop]

        # V0, a, b, c, d
        guess =  -0.04,0.01, 0.1, 0.01, 0.01
        #opt, cov = curve_fit(vDynModel,transient_time,transient_voltage,maxfev = 15000,p0=guess)
        param_bounds=([-0.09, 0, 0.1, 0, 0.01], [-0.03, 0.2, 0.11, 0.2, 0.011])
        opt, cov = curve_fit(vDynModel,transient_time,transient_voltage,maxfev = 1500,p0=guess,bounds=param_bounds)


        newOpt=np.append(opt,df_transient['SOC'].loc[start])
        coeff[i]=newOpt


        newOpt=np.append(opt,df_transient['SOC'].loc[start])
        coeff[i]=newOpt
        coeffDf = pd.DataFrame(coeff,columns=['V0','a','b','c','d','SOC'])


    #EXTRACT PARAMERTERS
    print('/!\ PARAMETER EXTRACTION...')
    maxTransients=len(start_index)
    #maxTransients=5
    parameters= np.zeros((maxTransients, 11))
    for i in range(0,maxTransients):
        start=start_index[i]
        stop=stop_index[i]

        i_start=df_transient.index.get_loc(start)
        i_stop=df_transient.index.get_loc(stop)   

        di=df_transient['dI'].iloc[i_start+1]
        transient_time=df_transient['time'].loc[start:stop]
        transient_time=transient_time.diff().dt.total_seconds().cumsum()
        transient_time=transient_time.replace(np.nan,0)

        dt=transient_time.loc[stop]

        dv=df_transient['dV'].iloc[i_start+1]

        R0=dv/di
        V0,a,b,c,d,SOC= coeff[i]


        tau1=1/b
        tau2=1/d
        #R1=a/((1 - np.exp(-dt / tau1)) * di)
        #R2=c/((1 - np.exp(-dt / tau1)) * di)

        R1=-a/di
        R2=-c/di

        C1=tau1/R1
        C2=tau2/R2

        parameters[i]=SOC,R0,R1,R2,C1,C2,tau1,tau2,di,dt,dv
            
    RxCx_parameters= pd.DataFrame(parameters,columns=['SOC','R0','R1','R2','C1','C2','tau1','tau2','di','dt','dv'])
    RxCx_parameters = RxCx_parameters.set_index("SOC")
    return RxCx_parameters
def relaxationDetector(row):
    start=0
    end=0
    if abs(row['dI']) >0.5  and abs(row['Current'])<0.001:
        start = 1
        dI=row['dI']
        V=row['Voltage']
    elif abs(row['dI']) >0.5  and abs(row['Current'])>0.1:
        end = 1
        dI=row['dI']
        V=row['Voltage']
    else:
        start =0
        end = 0
        dI=None
        V=None
        
    return start,end,dI,V
BETA_FACTORY_API= 'https://beta.altergo.io/'
BETA_IOT_API= 'https://iot.beta.altergo.io/'
apiKey = "65a0af318c601786c926ebfb8c7c89de"
edApi = eapi.Client(apiKey, BETA_FACTORY_API, BETA_IOT_API)
activities = None

if activityId < 0:
    pass
else:
    activities = [edApi.getActivityById(activityId)]

if activities is not None:
    pass
else:
    if activityName != "":
        activities = edApi.getActivitiesByName(activityName)
    else:
        print("Parameters are invalid")
for activity in activities:

    for asset in activity.assets:
        modelName = asset.model.name
        serialNumber = asset.serial_number


        if modelName == "INR-21700-P42A":
            startDate = activity.startDate
            endDate = activity.endDate
            
            print(startDate)
            
            altergoAsset = edApi.getAsset(serialNumber)
            print(altergoAsset)
            edApi.getAssetDataFrame([altergoAsset], ["Current", "Voltage"], startDate, endDate)
            assetParameters = extractParameters(altergoAsset.df)

            startDateString = datetime.utcfromtimestamp(startDate/1000).strftime('%Y-%m-%d %H:%M:%S')
            endDateString = datetime.utcfromtimestamp(endDate/1000).strftime('%Y-%m-%d %H:%M:%S')
            
            dataSetName = "HPPC-RC-Parameters-" + serialNumber 
            description = "HPPC TEST on "+ serialNumber+ " From " +startDateString + " To " + endDateString
            assetParameters = assetParameters.reset_index()
            edApi.uploadDatasetToAsset(altergoAsset, assetParameters,description, dataSetName, "csv", dataSetName)
            edApi.uploadDatasetToActivity(activity, assetParameters,description, dataSetName, "csv", dataSetName)

for activity in activities:     
    print(activity)
