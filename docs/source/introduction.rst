Introduction to Ion SDK
========================

Ion SDK is the software development kit associated with the Edison platform. The Ion SDK allows users to interact with the Edison platform to add, modify, interact or remove assets on the Edison platform from a Python environment.

Using the SDK, users can freely interact with their assets and the associated data on Edison without having to ever leave the Python environment.

To help users get started with the Ion SDK, we have set up a :ref:`quickstart guide <installation:Installation>`. Further, we have also created an :ref:`example <quickstart/edison_api_example:Edison API Basic Example>` to allow users to get started. Along with quickstarts and examples, we have also extensively documented the :ref:`codebase <reference/ion_sdk.edison_api:Edison API Reference Documentation>`.

.. toctree::
    :maxdepth: 4
    :hidden:

    introduction/who_uses
    introduction/def
    introduction/features
    introduction/use_cases
