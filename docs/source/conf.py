# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import furo
import shutil

sys.path.insert(0, os.path.abspath("../../"))


# -- Project information -----------------------------------------------------

project = "Ion SDK"
copyright = "2021, Ion Energy Inc"
author = "Ion Energy Inc"

# The full version, including alpha/beta/rc tags
release = "0.2"

# To ensure the nbsphinx properly gets the notebooks scattered across the
# entire build, include the following:

# examples_source = os.path.abspath(
#     os.path.join(os.path.dirname(__file__), "images/")
# )
# examples_dest = os.path.abspath(
#     os.path.join(os.path.dirname(__file__), "quickstart/notebooks")
# )

# if os.path.exists(examples_dest):
#     shutil.rmtree(examples_dest)
# os.mkdir(examples_dest)

# for root, dirs, files in os.walk(examples_source):
#     for dr in dirs:
#         os.mkdir(
#             os.path.join(root.replace(examples_source, examples_dest), dr)
#         )
#     for fil in files:
#         if os.path.splitext(fil)[1] in [".ipynb"]:
#             source_filename = os.path.join(root, fil)
#             dest_filename = source_filename.replace(
#                 examples_source, examples_dest
#             )
#             shutil.copyfile(source_filename, dest_filename)


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.napoleon",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
]

# The name of the Pygments (syntax highlighting) style to use.
# pygments_style = "sphinx"

# Source Suffixes

source_suffix = {
    ".rst": "restructuredtext",
    # '.txt': 'restructuredtext',
    ".md": "markdown",
}
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "_build",
    "Thumbs.db",
    ".DS_Store",
    "ztests.rst",
    "main.rst",
    "setup.rst",
    "modules.rst",
]

# Make sure the autosectionlabel target is unique
autosectionlabel_prefix_document = True

# Intersphinx references

intersphinx_mapping = {
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
    "numpy": ("http://docs.scipy.org/doc/numpy/", None),
    "pandas": ("http://pandas.pydata.org/pandas-docs/dev", None),
    "python": ("https://docs.python.org/3", None),
}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"
# html_logo = "images/logo_ion_energy_black.png"
html_theme_options = {
    "light_logo": "logo_ion_energy_black.png",
    "dark_logo": "logo_ion_energy.png",
    "sidebar_hide_name": False,
}
html_title = "Ion SDK"
# html_theme_path = [furo.get_html_theme_path()]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# -- Options for LaTeX PDF output --------------------------------------------

latex_logo = "_static/logo_ion_energy_black.png"
# latex_docclass = {
#     "howto":  #Insert path to docclass here
#     "manual": #Insert path to docclass here
# }
