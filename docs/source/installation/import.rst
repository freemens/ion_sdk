Importing Ion SDK into Python
------------------------------

To use the Ion SDK within a Python environment, it is important to first install the SDK. After completing the installation, the SDK can be imported into the Python environment for use.

To import the SDK into Python, include the following line along with other import statements:

.. code-block:: python

    >>> import ion_sdk

The Ion SDK has a number of subpackages. In the current version, Edison API is documented. Other subpackages will be documented in a future version.

To import only the Edison API, insert the following code along with other import statements:

.. code-block::

    >>> from ion_sdk.edison_api import edison_api

The above code will allow you to freely use the Edison API within your code.
