How to Install Ion SDK
-----------------------

You can install Ion SDK using the pip command within a bash terminal or a command prompt.

For a system-wide installation, you can use:

.. code-block::

    pip install git+https://bitbucket.org/freemens/ion_sdk.git@master

For a user-specific installation, you can use:

.. code-block::

    pip install --user git+https://bitbucket.org/freemens/ion_sdk.git@master

If you have already have an older version of the Ion SDK installed, you can upgrade it using:

.. code-block::

    pip install --upgrade git+https://bitbucket.org/freemens/ion_sdk.git@master

If you wish to install a particular version of the Ion SDK, you can do using:

.. code-block::

    pip install git+https://bitbucket.ord/freemens/ion_sdk.git@master=="version"

.. note::

    Drop the double inverted commas and replace version with the specific version that you wish to install.
