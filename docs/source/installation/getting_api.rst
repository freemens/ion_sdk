Connecting to the Edison Platform using your API Key
-----------------------------------------------------

After importing the Ion SDK, we can now setup a :class:`~ion_sdk.edison_api.edison_api.Client` to communicate with Edison. To setup a :class:`~ion_sdk.edison_api.edison_api.Client`, it is first important to obtain your API key which allows you to communicate with the Edison platform. Your API key can be found `here <https://edison.ionenergy.co/core/users/me/api-key>`_ or you can login to the Edison website and then **Click on Your Name in the top right corner > My Profile > Account Information**.

.. note::

    The API key is specific to your account and provides access to the Edison platform as defined by the adminstrator of the company within Edison.

Once you have the API Key, we can setup a :class:`~ion_sdk.edison_api.edison_api.Client` using the following command

.. code-block:: python

    >>> from ion_sdk.edison_api.edison_api import Client
    >>> edApi = Client(yourApiKeyHere)

The :class:`~ion_sdk.edison_api.edison_api.Client` class contains several methods to interact with the Edison platform. Thus, it would make sense to assign the class to a variable (e.g. :code:`edApi` in the above example).
