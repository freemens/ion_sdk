Pre-Requisities to Ion SDK
----------------------------

The Ion SDK has been designed to allow users with basic knowledge of Python to leverage the capabilities of the Ion SDK. Thus, a user need not be fluent in Python to utilize the Ion SDK. If you are unfamiliar with Python, we recommend you follow the `Official Python Documentation <https://docs.python.org>`_.

Before starting with the contents of the Ion SDK, users need to have an account on the Edison platform. If you do not have an account on the Edison platform, you can ask your company account administrator to give you one.

If you are using the Ion SDK for the first time, we recommend you follow the next pages to quickly have the SDK set up on your system. Once you have set up the Ion SDK on your system, you can refer to the :ref:`example <quickstart/edison_api_example:Edison API Basic Example>` to go over some of the common methods used during a workflow.

If you are familiar with the Ion SDK and wish to know the exact input arguments and outputs for a particular class, function, or method, you can refer to the :ref:`Reference Documentation <reference/ion_sdk.edison_api:Edison API Reference Documentation>`.

From the Quickstart Guide, one should be able to:

- Set up the Ion SDK on their local machine.
- Import the Ion SDK into your code.
- Obtain their API Key from the Edison platform.
