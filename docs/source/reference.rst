Reference
==========

Currently, the Ion SDK contains the following packages:

Edison API Reference Documentation
------------------------------------

This is the reference documentation for Edison API. Here, you will find all the automatically generated documentation based on the docstrings within the code.

.. automodule:: ion_sdk.edison_api.edison_api
   :members:
   :show-inheritance:
   :exclude-members: EdisonConsumer, raiseApiError, raiseApiResponse

..
.. toctree::
   :maxdepth: 1
   :hidden:
..
   reference/ion_sdk.edison_api
..   ion_sdk.models
..   ion_sdk.tools
