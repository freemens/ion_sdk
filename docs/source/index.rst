.. Ion SDK documentation master file, created by
   sphinx-quickstart on Tue Mar 30 19:19:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Ion Software Development Kit Offcial Documentation
===================================================

Welcome to the Ion Software Development Kit (Ion SDK) Official Documentation! We have created the Software Development Kit to aid you in fully leveraging the Edison platform. We invite you to explore the capabilities offered within the Software Development Kit! We hope you will find this document useful for your use cases. Further, we welcome you to raise issues and feature requests to improve the software development kit.

To help users get started with the Ion SDK, we have described the steps for :ref:`installation <installation:Installation>`. Further, we have also created an :ref:`example <quickstart/edison_api_example:Edison API Basic Example>` to allow users to get started. Along with examples, we have also extensively documented the codebase in the :ref:`References <reference/ion_sdk.edison_api:Edison API Reference Documentation>` section.

Who Uses the Ion SDK?
----------------------

The Ion SDK is developed for users who wish to perform data analysis using their algorithms and analytics while leveraging the scalability of the Edison platform. By utilizing internal algorithms or analytics in combination with the Edison platform, the growth can be compounded upon without exposing the algorithms or analytics to Edison.

Key Definitions
-----------------

Before getting into the documentation, we believe that it is important for the user to understand the meaning of some of the common words and/or phrases used across the documentation. To that end, we have collated a list of such terms along with their definitions. We would advise new users of the Ion SDK to familiarize themselves with these terms as they often come up throughout the documentation.

- **Edison**: The Edison platform where all your models, assets, and data are stored.

- **Edison API**: The API responsible for communicating with the Edison platform from Python. The Edison API is a part of the Ion SDK.

- **Client**: The Client is part of the Edison API that contains several useful methods to interact with the Edison platform.

- **API Key**: The API Key is a unique identifier for your Edison account that allows you to interact with the assets within your company.

- **Model**: Blueprint structure of a physical or virtual system that can be used to instantiate several assets.

- **Asset**: A physical or virtual object that is an instance of the model. The asset contains data related to the sensors as defined in the model.

Features covered in Ion SDK
----------------------------

The Ion SDK offers several features. In the current version, the SDK provides access to the Edison platform through the Edison API.

Access to the Edison platform through the Edison API allows users to:

- Instantiate, modify, and delete assets on the Edison platform.
- Ingest and attach data to sensors that are associated with a particular asset. (You can use an existing API or upload a CSV file).
- Establish a two-way communication with the Edison platform to input, analyze, and output data.

Additional functionalities will be added in a future version.

Use-Cases for Ion SDK
-----------------------

Ion SDK provides a seamless way to interact with your assets within the Edison platform. Users can leverage the SDK for several use-cases:

- Algorithm Testing (e.g. Finding the best disptach strategy, etc.)
- Optimization (e.g. Minimizing degradation during operation, reducing the time to charge batteries while minimizing degradation, etc.)
- Data Analytics (e.g. Pattern Recognition, Feature Extraction, Outlier Detection, Warranty Checks, etc.)
- Machine Learning/AI (e.g. End of Life Prediction, Predictive Maintenance, etc.)

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   :hidden:

   installation
   quickstart
   reference
   feedback
