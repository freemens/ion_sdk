Quickstart
===========

Currently, the Ion SDK is geared towards accessing the Edison platform through various APIs. This is made possible through the Edison API.

For an absolute basic start to Edison API, please refer to the :ref:`Edison API Basic Example <quickstart/edison_api_example:Edison API Basic Example>`.

For the complete reference documentation for Edison API, please refer to the :ref:`Edison API Reference Documentation <reference/ion_sdk.edison_api:Edison API Reference Documentation>`.

.. toctree::
   :maxdepth: 1
   :hidden:

   quickstart/edison_api_example
   quickstart/end_to_end_workflow
