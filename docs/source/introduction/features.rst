Features covered in Ion SDK
----------------------------

The Ion SDK offers several features. In the current version, the SDK provides access to the Edison platform through the Edison API.

Access to the Edison platform through the Edison API allows users to:

- Instantiate, modify, and delete assets on the Edison platform.
- Ingest and attach data to sensors that are associated with a particular asset. (You can use an existing API or upload a CSV file).
- Establish a two-way communication with the Edison platform to input, analyze, and output data.

Additional functionalities will be added in a future version.
