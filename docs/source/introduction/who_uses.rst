Who Uses the Ion SDK?
----------------------

The Ion SDK is developed for users who wish to perform data analysis using their algorithms and analytics while leveraging the scalability of the Edison platform. By utilizing internal algorithms or analytics in combination with the Edison platform, the growth can be compounded upon without exposing the algorithms or analytics to Edison.
