Key Definitions
================

Before getting into the documentation, we believe that it is important for the user to understand the meaning of some of the common words and/or phrases used across the documentation. To that end, we have collated a list of such terms along with their definitions. We would advise new users of the Ion SDK to familiarize themselves with these terms as they often come up throughout the documentation.

- **Edison**: The Edison platform where all your models, assets, and data are stored.

- **Edison API**: The API responsible for communicating with the Edison platform from Python. The Edison API is a part of the Ion SDK.

- **Client**: The Client is part of the Edison API that contains several useful methods to interact with the Edison platform.

- **API Key**: The API Key is a unique identifier for your Edison account that allows you to interact with the assets within your company.

- **Model**: Blueprint structure of a physical or virtual system that can be used to instantiate several assets.

- **Asset**: A physical or virtual object that is an instance of the model. The asset contains data related to the sensors as defined in the model.
