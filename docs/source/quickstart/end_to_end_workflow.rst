End to End Workflow
--------------------

This example shows users how to use the Edison platform and the ION SDK to perform analysis within Python while using data from Edison. For this example use-case, we will explore how a user with data from an ISO and batteries performs analyses to decide which dispatch strategy would be optimum.

Set up of models on Edison
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For this example, the model for an ISO node and a battery is created on Edison. The steps to create a model are described in the previous :ref:`example <quickstart/edison_api_example:Adding models on Edison>`. The ISO model is provided with a sensor that records the day-ahead energy prices. The battery is given parameters of voltage, capacity, and maximum power rating.

.. image:: ../_static/EndToEndExampleModel.png

Importing data into Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To import data into Python, we first import the required assets. To import assets from Edison, we need to first set up a :class:`~ion_sdk.edison_api.edison_api.Client`. We will make use of the :func:`~ion_sdk.edison_api.edison_api.setupClient` function to create a :class:`~ion_sdk.edison_api.edison_api.Client`.

.. code-block:: python

    >>> from ion_sdk.edison_api.edison_api import setupClient
    >>> edApi = setupClient()

Once we have a Client, we can use the :meth:`~ion_sdk.edison_api.edison_api.Client.getAsset` method to get assets into Python.

.. code-block:: python

    >>> iso_asset = edApi.getAsset("ISO_Asset")
    >>> battery_asset1 = edApi.getAsset("Battery_Asset1")
    >>> battery_asset2 = edApi.getAsset("Battery_Asset2")

Now, we can use the :meth:`~ion_sdk.edison_api.edison_api.Client.getAssetDataFrame` to pull the data from the required sensor. In our case, we only need data from the ``iso_asset``.

.. code-block:: python

    >>> from ion_sdk.edison_api.edison_api import edisonDate
    >>> edApi.getAssetDataFrame([iso_asset], ["DAM Energy LMP"], edisonDate(2021,1,1,0,0,0), edisonDate(2021,2,1,0,0,0))

The above method attaches a :class:`~pandas.DataFrame` to the ``iso_asset`` through an attribute ``df``. Thus, data has been imported into Python.

Performing Analysis
^^^^^^^^^^^^^^^^^^^^^

For this example, consider we have two algorithms- ``strategy1`` and ``strategy2`` which perform some analysis on this data. The analysis reveals an ideal dispatch schedule for the battery to maximize a key performance metric.

.. code-block:: python

    >>> df_1 = strategy1(iso_asset, battery_asset)
    >>> df_2 = strategy2(iso_asset, battery_asset)

After the analysis is complete and we have the ideal dispatch schedule, we can send this off the Edison for better visualization and decision-making.

Sending Data to Edison
^^^^^^^^^^^^^^^^^^^^^^^

To send data back to Edison, we first have to attach the schedule to the battery assets.

.. code-block:: python

    >>> battery_asset1.df = df_1
    >>> battery_asset2.df = df_2

After attaching the data to the asset, we can send the data to Edison using the :meth:`~ion_sdk.edison_api.edison_api.Client.updateSensorDataByFile` method. The method requires an ``asset``, and the sensor list to update.

.. note::

    The names of the sensors associated with the asset must match the column names of the :class:`~pandas.DataFrame`.

.. code-block:: python

    >>> edApi.updateSensorDataByFile(battery_asset1, ["P_in", "P_out", "SOC"])
    >>> edApi.updateSensorDataByFile(battery_asset2, ["P_in", "P_out", "SOC"])

Now, you can leverage the dashboards on Edison to visualize the data and make important decisions.
