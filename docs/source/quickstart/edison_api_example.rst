Edison API Basic Example
-------------------------

Welcome! Here, you will find a basic example that goes over the capabilities of the Edison API.

The example is broken down into several smaller sections to help users identify methods they need to call to perform certain tasks.

Installing Ion SDK
^^^^^^^^^^^^^^^^^^^

The first step towards using the Ion SDK is installing the SDK. You can refer to the quickstart guide for installation instructions. Alternatively, you can run the following command in a bash terminal or command prompt.

.. code-block::

    pip install --user git+https://bitbucket.org/freemens/ion_sdk.git@master``

Importing the Ion SDK
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After installing the Ion SDK on your local machine, you can import the SDK into your Python environment using the ``import`` command in Python.

.. code-block:: python

    >>> import ion_sdk

Setting up an Edison API Client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To access the Edison platform from Ion SDK, we need to set up an API Client with Edison. To set up this client, we need to provide our API Key to the :class:`~ion_sdk.edison_api.edison_api.Client`.

.. code-block:: python

    >>> from ion_sdk.edison_api.edison_api import setupClient, edisonDate
    >>> edApi = setupClient()

Once we have set up the client, we can now communicate with the Edison platform to perform several tasks. Let us go over them one by one.

Sample Tasks
^^^^^^^^^^^^^

Adding models on Edison
""""""""""""""""""""""""

Models on the Edison platform form the blueprints from which an asset is instantiated. Each model describes parameters, properties, and sensors that are going to be associated with it.

Steps to create a model on Edison:

#. Log on to the Edison platform. Once on the platform, Using the side-menu, navigate to the model page: **Designer > Model** on the top right of the window click on **Add New Model** you will land in the model designer wizard.

    .. image:: ../_static/EdisonModelGeneral.png

#. To create and save a model, these fields must be filled out:
    - Name: it will allow you to find it in the browser. Choose a relevant name such as a part number, avoid using brands or generic name
    - Category: This step-by-step tutorial focuses on battery model so we’ll choose “Battery” as our category

    .. admonition:: Info

        While not mandatory, attaching an image of your model is recommended to facilitate browsing. Image size shouldn't exceed 200x120 pixels for optimal display

#. In the Specifications section, we can add the parameters associated with the model. In this example, we define a parameter ``Capacity`` with a nominal value of 3.6Ah. In general, a parameter is composed of:
    - A Name with an associated unit in S.I (if relevant)
    - A set of values with:
    - A Prefix [Min,Max,Nom…] (if relevant)
    - A Value [number, text]
    - A Multiplier [p,n,u…,k,M,G] (if relevant)

    .. image:: ../_static/EdisonModelSpecifications.png

#. In the System Architecture section, we can multiple sub-components associated with the model. These sub-componenets can be modeled using the same model designer. In this example, we do not have any sub-components. In general, sub-components are described by:
    - A Name with an associated with a pre-existing model
    - A Quantity
    - A Virtual state, to describe if the sub-component is physically tracked or mentioned to provide additional context.

#. In the Sensors section, we can include all the sensors that record metrics logged during the sensor's life. This is an important step that will be critical in order to use the Visualization Tool. Sensors are defined by 3 characteristics:
    - The name.
    - The array size. Most of the time, the array size is equal to 1 except for specific sensors such as cell voltage, temperature array, current array, etc.
    - The array map. It defines a map that will be used by Edison to set a meaningful name to your sensor array elements. For example, a sensor that would track an array of flags needs a map that would designate each flag. Example:

    .. code-block:: bash

        ["Precharge relay","Discharge relay","Charge relay","Fan relay","IsoI0","IsoI1","StartReq","Wifi/Bt Act","RunnningMode","Balancing","ConfLoaded","External power supply state","Isolated CAN supply state","Isolated CAN keep alive received","Sd conf file opening error","Sd conf file param error","Sd init flag","No error detected","CO dep ignition request","Connection to DC bus allowed","PPC is manageable","System battery up to date","System has no error","Is in power allowed area","Glimpse wake up","Sleep wake up","Charger wake up","Wake up for balancing","Can wake up","Bluetooth wake up","Ignition wake up","Parallel ignition wake up"]

    To add sensors to your model, follow these steps:

        - Click on the “Add” button.
        - Search and select the sensor you wish to add.
        - Specify the array size.
        - Specify the array map.

    In this example, we add a voltage sensor to our model.

    .. image:: ../_static/EdisonModelSensor.png

    To create sensors for your models, follow these steps:

        - Navigate to the **Designer > Sensors** page. On the page, click on the **Add New Sensor**.
        - Add a name for your sensor. If it aligns with one of the types, select the type from the dropdown menu.
        - Add the units for your sensor.
        - Add a multiplier for your sensor readings, if any.
        - Click on the submit button to save the sensor and make it available for use in a model.

    .. image:: ../_static/EdisonSensorPage.png

#. In the Properties section, include any additional properties.

#. In the Managers section, add names of people from partner companies who can read/write your models.

    .. note::

        You can skip this step if you do not have a partner company.

#. Click on the submit button to submit the model.

You can now find this model in the list of models on the Models page. You can refer to the `Edison documentation <https://edison.ionenergy.co/core/doc/docs/edison/guides/edison-orientation/creating-battery-model>`_ for greater details on building a model.

.. image:: ../_static/EdisonModelPage.png

Create an asset
""""""""""""""""

By using the :meth:`~ion_sdk.edison_api.edison_api.Client.createAssetBySerial` method within the :class:`~ion_sdk.edison_api.edison_api.Client`, we can create an ``asset`` out of available models previously defined on Edison. For the example, a model called ``Foo`` has been defined on Edison (Refer to the image above).

.. code-block:: python

    >>> model_name = "Foo"
    >>> serial_number = "Bar"
    >>> edApi.createAssetBySerial(model_name, serial_number)

Get an asset
"""""""""""""

After creating a component, let us try to bring it into our Python environment.

.. code-block:: python

    >>> asset = edApi.getAsset("Bar")

For this example, the model was set up with a parameter called ``Capacity`` with a value of 3.6Ah. The model was given a ``Voltage`` sensor.

Adding data to an Asset
""""""""""""""""""""""""

To add data to an asset, it is first important to get the data in the correct format. The best way to upload data to Edison is by using a :class:`pandas.DataFrame`. The format of the :class:`~pandas.DataFrame` must be as follows:

- The index of the :class:`~pandas.DataFrame` must be :class:`~pandas.DatetimeIndex`. The index tells Edison about the timestamp of the data record.
- Each column name should correspond to one of the sensors associated with the model. If there is an error in any of the column names, an ``Error`` will be raised and the data will not be sent to Edison.
- Entries in the columns are to be numerical in nature.

Once such a :class:`~pandas.DataFrame` is created, it can be assigned to the ``asset`` as an attribute. An ``Asset`` stores :class:`DataFrames <pandas.DataFrame>` in the attribute ``df``. Thus, we would want to do the following:

.. code-block:: python

    >>> import pandas as pd
    >>> import numpy as np
    >>> np.random.seed(1234)
    >>> example_df = pd.DataFrame(
    >>>     {
    >>>         "Voltage":[3.6 + 0.01 * np.random.rand() for _ in range(1, 11)]
    >>>     },
    >>>     index = pd.date_range(
    >>>         start= '2021-04-15',
    >>>        periods= 10,
    >>>         freq= 'S',
    >>>         name= "time"
    >>>     )
    >>> )

As described before, we now assign the ``example_df`` to ``asset.df``.

.. code-block:: python

    >>> asset.df = example_df

Sending data to Edison
"""""""""""""""""""""""

After adding the data to ``asset.df``, we can add this data to Edison using the :meth:`~ion_sdk.edison_api.edison_api.Client.updateSensorDataByFile` method. The arguments required for the method are the asset and the list of sensors we want to update. In the example, since we only have one sensor (``Voltage``), we will send data to this sensor only.

.. code-block:: python

    >>> uploadSensorList = ["Voltage"]
    >>> edApi.updateSensorDataByFile(asset, uploadSensorList)

.. note::

    The names of the sensors in the ``uploadSensorList``, and the one associated with the Edison model **must** match. Also, the name **must** match with the name of the column in ``asset.df``.

.. warning::

    Pre-existing data on Edison within the timeframe described by the :class:`~pandas.DataFrame` will **erased** and **cannot** be retrieved.

We have now successfully uploaded data to the ``asset``.

Retrieving data from Edison
""""""""""""""""""""""""""""

To retrieve data from Edison, we need to first get the ``asset`` into our Python environment. To do so, we can use the :meth:`~ion_sdk.edison_api.edison_api.Client.getAsset` method as described in :ref:`Get an asset <quickstart/edison_api_example:Get an asset>`.

After retrieving the asset, we can then get data from the asset. To get data from the asset, we use the :meth:`~ion_sdk.edison_api.edison_api.Client.getAssetDataFrame` method. This method assigns the data we require as a :class:`~pandas.DataFrame` to the ``df`` attribute.

To select what data we want, we provide the asset, the sensors of interest, and the start and end date of the time-period of interest to the :meth:`~ion_sdk.edison_api.edison_api.Client.getAssetDataFrame` method.

.. code-block:: python

    >>> edApi.getAsset(serial_number)
    >>> sensors = ["Voltage"]
    >>> edApi.getAssetDataFrame(
    >>>         [asset],
    >>>         sensors,
    >>>         edisonDate(2021, 4, 15, 0, 0, 0),
    >>>         edisonDate(2021, 4, 15, 0, 0, 10)
    >>>         )

.. note::

    The names of the sensors in the ``sensors``, and the one associated with the Edison model **must** match.

After retrieving the data of interest, several analyses can be performed within the Python environment. After completion of the analysis, the newly created data can be sent to Edison using the :meth:`~ion_sdk.edison_api.edison_api.Client.updateSensorDataByFile` method as described in :ref:`Sending data to Edison <quickstart/edison_api_example:Get an asset>`.

Delete an asset
""""""""""""""""

If at any point, an ``asset`` needs to be deleted from the Edison database, we can do so using the :meth:`~ion_sdk.edison_api.edison_api.Client.deleteAssetBySerial` method.

.. code-block:: python

    >>> edApi.deleteAssetBySerial(serial_number)

.. warning::

    Once an ``asset`` is deleted, it **cannot** be undone.
