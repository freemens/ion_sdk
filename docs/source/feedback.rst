Feedback and Report Issues
===========================

You can send raise feature requests, feedback, and report issues in the `Issues <https://bitbucket.org/freemens/ion_sdk/issues>`_ for the Ion SDK repository.

Issue description etiquette
****************************

**Mandatory**

- Context: *When / Where/ on Which version did the issue happen?*
- Trigger: *What did you do just before?*
- Expectation: *What did you expect after the Trigger?*
- Reality: *What happened instead?*
- Reproducible: *Can you reproduce this use-case?*

**Nice to have**

- Screenshots
- Code snippets
