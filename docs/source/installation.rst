Installation
=============

The following quickstart guide takes you through the basic steps to set up the Ion SDK within your Python environment and using it to interact with the Edison platform.

Pre-Requisities to Ion SDK
----------------------------

The Ion SDK has been designed to allow users with basic knowledge of Python to leverage the capabilities of the Ion SDK. Thus, a user need not be fluent in Python to utilize the Ion SDK. If you are unfamiliar with Python, we recommend you follow the `Official Python Documentation <https://docs.python.org>`_.

Before starting with the contents of the Ion SDK, users need to have an account on the Edison platform. If you do not have an account on the Edison platform, you can ask your company account administrator to give you one.

If you are using the Ion SDK for the first time, we recommend you follow the next pages to quickly have the SDK set up on your system. Once you have set up the Ion SDK on your system, you can refer to the :ref:`example <quickstart/edison_api_example:Edison API Basic Example>` to go over some of the common methods used during a workflow.

If you are familiar with the Ion SDK and wish to know the exact input arguments and outputs for a particular class, function, or method, you can refer to the :ref:`Reference Documentation <reference/ion_sdk.edison_api:Edison API Reference Documentation>`.

From the Quickstart Guide, one should be able to:

- Set up the Ion SDK on their local machine.
- Import the Ion SDK into your code.
- Obtain their API Key from the Edison platform.

How to Install Ion SDK
-----------------------

You can install Ion SDK using the pip command within a bash terminal or a command prompt.

For a system-wide installation, you can use:

.. code-block::

    pip install git+https://bitbucket.org/freemens/ion_sdk.git@master

For a user-specific installation, you can use:

.. code-block::

    pip install --user git+https://bitbucket.org/freemens/ion_sdk.git@master

If you have already have an older version of the Ion SDK installed, you can upgrade it using:

.. code-block::

    pip install --upgrade git+https://bitbucket.org/freemens/ion_sdk.git@master

If you wish to install a particular version of the Ion SDK, you can do using:

.. code-block::

    pip install git+https://bitbucket.ord/freemens/ion_sdk.git@master=="version"

.. note::

    Drop the double inverted commas and replace version with the specific version that you wish to install.

Importing Ion SDK into Python
------------------------------

To use the Ion SDK within a Python environment, it is important to first install the SDK. After completing the installation, the SDK can be imported into the Python environment for use.

To import the SDK into Python, include the following line along with other import statements:

.. code-block:: python

    >>> import ion_sdk

The Ion SDK has a number of subpackages. In the current version, Edison API is documented. Other subpackages will be documented in a future version.

To import only the Edison API, insert the following code along with other import statements:

.. code-block::

    >>> from ion_sdk.edison_api import edison_api

The above code will allow you to freely use the Edison API within your code.

Connecting to the Edison Platform using your API Key
-----------------------------------------------------

After importing the Ion SDK, we can now setup a :class:`~ion_sdk.edison_api.edison_api.Client` to communicate with Edison. To setup a :class:`~ion_sdk.edison_api.edison_api.Client`, it is first important to obtain your API key which allows you to communicate with the Edison platform. Your API key can be found `here <https://edison.ionenergy.co/core/users/me/api-key>`_ or you can login to the Edison website and then **Click on Your Name in the top right corner > My Profile > Account Information**.

.. note::

    The API key is specific to your account and provides access to the Edison platform as defined by the adminstrator of the company within Edison.

Once you have the API Key, we can setup a :class:`~ion_sdk.edison_api.edison_api.Client` using the following command

.. code-block:: python

    >>> from ion_sdk.edison_api.edison_api import Client
    >>> edApi = Client(yourApiKeyHere)

The :class:`~ion_sdk.edison_api.edison_api.Client` class contains several methods to interact with the Edison platform. Thus, it would make sense to assign the class to a variable (e.g. :code:`edApi` in the above example).

..
.. toctree::
    :maxdepth: 2
    :hidden:
..
    installation/prereq
    installation/pip
    installation/import
    installation/getting_api
