from setuptools import setup, find_packages

# run python3 setup.py bdist_wheel

setup(
    name="ion_sdk",
    version="0.4.8a",
    packages=find_packages(),
    install_requires=[
        "uplink==0.9.7",
        "pandas",
        "numpy",
        "scipy",
        "python-dateutil",
        "tabulate",
        "matplotlib",
        "fuzzywuzzy",
        "plotly",
        "tqdm",
    ],
)
