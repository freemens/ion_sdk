# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_python37_app]
from flask import Flask
import pika
import os
import time
from threading import Thread
from ion_sdk.ztests.test_soh_model import calculateSoh
import json
import functools


def processSohCalculation(msg):
    try:
        task = json.loads(msg)
        calculateSoh(task)
    except Exception as e:
        print(e)

    return


def setupRabbitMq():
    print("Start rabbitMq thread")
    # Access the CLODUAMQP_URL environment variable and parse it (fallback to localhost)
    url = os.environ.get(
        "CLOUDAMQP_URL",
        "amqp://user:o6eeqrOWs70x@ec2-52-89-61-135.us-west-2.compute.amazonaws.com:5672",
    )
    # url = os.environ.get('CLOUDAMQP_URL', 'amqp://guest:guest@127.0.0.1:5672') # localhost
    params = pika.URLParameters(url)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()  # start a channel
    channel.queue_declare(
        queue="iot_sentinel_soh", durable=True
    )  # Declare a queue
    channel.basic_qos(prefetch_count=1)

    def ack_message(channel, delivery_tag):
        """Note that `channel` must be the same pika channel instance via which
        the message being ACKed was retrieved (AMQP protocol constraint).
        """
        if channel.is_open:
            channel.basic_ack(delivery_tag)
        else:
            # Channel is already closed, so we can't ACK this message;
            # log and/or do something that makes sense for your app in this case.
            pass

    def do_work(connection, channel, delivery_tag, body):
        # Sleeping to simulate 10 seconds of work
        print("Start SOH diagnosis ...")
        processSohCalculation(body)
        print("... SOH diagnosis ended")
        cb = functools.partial(ack_message, channel, delivery_tag)
        connection.add_callback_threadsafe(cb)

    def on_message(channel, method_frame, header_frame, body, args):
        (connection, threads) = args
        delivery_tag = method_frame.delivery_tag
        t = Thread(
            target=do_work, args=(connection, channel, delivery_tag, body)
        )
        t.start()

    threads = []
    # set up subscription on the queue
    on_message_callback = functools.partial(
        on_message, args=(connection, threads)
    )
    channel.basic_consume(
        "iot_sentinel_soh", on_message_callback, auto_ack=False
    )
    print("Consuming rabbitMq queue")
    channel.start_consuming()


t_msg = Thread(target=setupRabbitMq)
t_msg.start()

# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)


@app.route("/")
def hello():
    """Return a friendly HTTP greeting."""
    return "Hello World!"


if __name__ == "__main__":
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    print("Start flask server")
    app.run(host="127.0.0.1", port=8080, debug=False)
# [END gae_python37_app]
