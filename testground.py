
from ion_sdk.edison_api.edison_api import edisonDate
import ion_sdk.edison_api.edison_api as eapi
from ion_sdk.edison_api.models.factoryModel import EdisonGenericComponent, CurrentState, Tag, Model, Dashboard
from datetime import datetime, timedelta
from ion_sdk.tools.sim import Sensor
from ion_sdk.tools.toolbox import dataframeFromSensors
import pandas as pd
import glob
from tests.test_edison_api import test_ComponentApi, test_example_use_case
import json
from typing import List

# connect using API key
apiKey = '2e954dc04d062c8222daea1e38b5adde'
edApi = eapi.Client(apiKey, "http://localhost", "https://iot.staging.altergo.io/")

# create new asset
# model = Model()
# currentState = CurrentState()
# model.name = "Test Vehicle"
# newComp = EdisonGenericComponent()
# newComp.model = model
# newComp.serial_number = "TEST3 component"
# newComp.current_state = currentState
# newComp.current_state.description = "First upload"
# newAsset = edApi.createAsset(newComp)

# create new tag
# newTag = Tag()
# newTag.name = "Test tag"
# newTag.color = "danger"
# newTag.description = "test description"
# newTag = edApi.createTag(newTag)

# remove Tag
# try :
#     tagName = "Test tag"
#     tag = edApi.getTagByName(tagName)
#     response = edApi.removeTag(tag)
#     print(response)
# except:
#   print("Provide correct tag name")

# edit Tag
# try:
#     tag = edApi.getTagByName("Test tag")
#     editedTag = {
#         "name":"Edit tag",
#         "color":"warning",
#         "description":"working"
#         }
#     response = edApi.editTag(tag,editedTag)
#     print(response)
# except:
#     print("Provide correct tagname")


# link newly created tag to the new asset
# taglinkedassets = edApi.linkTagToAssets(newTag, newAsset['content'])


# create new dashboard
# newDashboard = Dashboard()
# newDashboard.name = "Test dashboard"
# newDashboard.tags = [newTag]
# newDashboard.projectId = 1
# newDashboard.iconClass = "la la-bug"
# newDashboard.iconBgColor = "warning"
# newDashboard.targetedTemplateId = 4
# newDashboard.accessibility = "Public"
# newDashboard.isTemplate = True

# dashboard = edApi.createDashboard(newDashboard)


# edit Dashboard
# try:
#     dashboards = edApi.getDashboardsByName(newDashboard.name)
#     editedDashboard = {
#             "name":"Edited Dashboard", 
#             "tags":[newTag.name],
#             "projectId":"2",
#             "iconClass":"la la-pie-chart",
#             "iconBgColor":"dark",
#             "accessibility":"PUBLIC",
#             "isTemplate":True
#         }
#     response = edApi.editDashboard(Dashboard.from_dict(dashboards["data"][0]),editedDashboard)
#     print(response)
# except:
#     print("Provide correct dashboardId")


# remove dashboard

# try:
#     dashboards = edApi.getDashboardsByName(dashboard.name)

#     if dashboards is not None:

#         dashboardList = []
#         for dashboard in dashboards["data"]:
#             dashboardList.append(Dashboard.from_dict(dashboard))

#         res = edApi.removeDashboards(dashboardList)

# except:
#   print("Provide correct dashboardId")
