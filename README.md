# Ion SDK

![Coverage Badge](https://bitbucket.org/freemens/ion_sdk/downloads/coverage.svg)

Ion SDK is the software development kit associated with the Edison platform. The Ion SDK allows users to interact with the Edison platform to add, modify, interact or remove assets on the Edison platform from a Python environment.

Using the SDK, users can freely interact with their assets and the associated data on Edison without having to ever leave the Python environment.

## Installation

### Environment setup

The repository is architectured to run in a virtual environement. 
We advise the use of [pipenv ](https://pipenv.pypa.io/en/latest/#install-pipenv-today)
```bash

pip install pipenv

```

Once pipenv is installed, open a terminal in the repository and run : 

```bash

pipenv shell

```
Then 

```bash

pipenv install

```

This should ensure that the dependancies are installed in a contained virtual environement. 
If using vscode make sure that the Python environement is the one you just created. 
This requires a restart of vscode sometimes. 

### API Endpoints

You can install Ion SDK using the pip command within a bash terminal or a command prompt. Use the following command to install:

    pip install git+https://bitbucket.org/freemens/ion_sdk.git@master

After the installation, add ``ALTERGO_FACTORY_API`` and ``ALTERGO_IOT_API`` to the environment variables.

For Windows users, run the following commands in a command prompt:

    setx ALTERGO_FACTORY_API "your\ altergo\ factory\ api\ endpoint\ here"
    setx ALTERGO_IOT_API "your\ altergo\ iot\ api\ endpoint\ here"

For UNIX users (Linux or macOS), add the following two lines to ``~/.bashrc`` or ``~/.zshrc`` (based on your preffered terminal):

    export ALTERGO_FACTORY_API=your\ altergo\ factory\ api\ endpoint\ here
    export ALTERGO_IOT_API=your\ altergo\ factory\ api\ endpoint\ here

For users on Google Colaboratory, or similar services, run the following lines in a terminal before importing Ion SDK:

    %env ALTERGO_FACTORY_API=your\ altergo\ factory\ api\ endpoint\ here
    %env ALTERGO_IOT_API=your\ altergo\ factory\ api\ endpoint\ here

## Documentation

You can refer the latest documentation for Ion SDK [here](https://ion-energy.gitlab.io/edison-kb-private/).

## Feature Requests, Issues, Bug Reports

You can send raise feature requests, feedback, and report issues in the [Issues](https://bitbucket.org/freemens/ion_sdk/issues) for the Ion SDK repository. Please give as much information as you can in the ticket. It is extremely useful if you can supply a small self-contained code snippet that reproduces the problem. Also specify the component, and the version you are referring to.
Thank you.
